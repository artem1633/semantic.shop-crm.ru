<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ключевики';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keywords-index">


    <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    <span class="btn btn-success" onClick="deleteSelectedTasks()"><i class="fa fa-trash-o"></i> Удалить</span>
    <?php
    \yii\bootstrap\Modal::begin([
        'header' => 'Объединить в группу',
        'toggleButton' => ['label' => '<i class="fa fa-pencil"></i> Объединить в группу',
            'tag' => 'button',
            'class' => 'btn btn-success',],
    ]);
    ?>
    <input type="text" name="group_name" id="group_name" placeholder="Название группы...">

    <span class="btn btn-success" onClick="group()">Объединить</span>

    <?php \yii\bootstrap\Modal::end(); ?>

    <p>
    </p>
    <div class="panel panel-default panel-body">

        <?php Pjax::begin(['id' => 'pjax_grid']); ?>

    <?= GridView::widget([
            'id' => 'grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\CheckboxColumn',
                'name' => 'checked',
                'checkboxOptions' => function($model) {
                    return ['value' => $model->id];
                },
                'contentOptions' => ['style' => 'text-align:center;'],
                'headerOptions' => ['style' => 'text-align:center;']
            ],
            'id',
            'keyword',
            'frequency',
            'status',
            [
                'attribute' => 'group_id',
                'content' => function($data) {
                    return isset($data->group_id) ? $data->group_id : '<span style="color:red">Не в группе</span>';
                },
                'contentOptions' => ['style' => 'text-align:center white-space: nowrap;;'],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'white-space: nowrap; '],
            ],
        ],
    ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>

<script type="text/javascript">

    function deleteSelectedTasks() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Ключевики',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Удалить выбранные Ключевик?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'delete-selected-tasks',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }



    function group() {

        var keys = $('#grid').yiiGridView('getSelectedRows');
        var group_name = $('#group_name').val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Ключевиков',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Объединить выбранные ключевики в группу?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'group',
                data: {
                    keylist: keys,
                    group_name: group_name
                },
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

</script>
