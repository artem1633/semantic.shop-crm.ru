<?php
use app\modules\admin\models\KeywordGroups;
use app\modules\admin\models\Task;

if ($this->context->route=='admin/keyword-groups/update') {
    $keywordsGroup = KeywordGroups::find()->where(['id'=>Yii::$app->request->get()])->one();        
    $status = $keywordsGroup->status;
}
if ($this->context->route=='admin/task/update') {
    $task = Task::find()->where(['id'=>Yii::$app->request->get()])->one();        
    $statusTask = $task->status;
}
?>

?>
<aside class="main-sidebar">

    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Админ. панель', 'options' => ['class' => 'header']],
                    ['label' => 'Проекты', 'icon' => 'book', 'url' => ['project/index']],
                    ['label' => 'ТЗ',
                        'icon' => 'calendar',
                        'items' => [
                            [
                                'label' => 'ТЗ',
                                'encode' => false,
                                'icon' => 'calendar',
                                'url' => ['task/projects-status'],
                                'active' => $this->context->route == 'admin/task/all-tasks' ||
                                    $this->context->route == 'admin/task/projects-status',
                            ],
                            [
                                'label' => 'Исходные ключевики',
                                'icon' => 'calendar-o',
                                'url' => ['task/projects-status', 'status' => '1'],
                                'active' => Yii::$app->request->get('status') == 1 ||
                                    $this->context->route == 'admin/task/status1' || (isset($statusTask) && $statusTask == Task::STATUS_SOURCE_KEYS),

                            ],
                            [
                                'label' => 'В работе',
                                'icon' => 'calendar-o',
                                'url' => ['task/projects-status', 'status' => '4'],
                                'active' => Yii::$app->request->get('status') == 4 ||
                                    $this->context->route == 'admin/task/status4' || (isset($statusTask) && $statusTask == Task::STATUS_IN_WORK),

                            ],
                            [
                                'label' => 'Готовое ТЗ',
                                'icon' => 'calendar-check-o',
                                'url' => ['task/projects-status', 'status' => '2'],
                                'active' => Yii::$app->request->get('status') == 2 ||
                                    $this->context->route == 'admin/task/status2'
                                    ||
                                    $this->context->route == 'admin/task/index'  || (isset($statusTask) && $statusTask == Task::STATUS_DONE),
                            ],
                            [
                                'label' => 'Использованные ТЗ',
                                'icon' => 'archive',
                                'url' => ['task/projects-status', 'status' => '3'],
                                'active' => Yii::$app->request->get('status') == 3 ||
                                    $this->context->route == 'admin/task/status3' || (isset($statusTask) && $statusTask == Task::STATUS_USED),
                            ],
                        ],
                    ],

                    ['label' =>'Семантика',
                        'icon' => 'calendar',
                        'options' => ['style' => 'display: block!important;'],
                        'items' => [
                            [
                                'label' => 'Исходные группы',
                                'url' => ['keyword-groups/source-groups'],
                                'active' => $this->context->route == 'admin/keyword-groups/source-groups' ||
                                    $this->context->route == 'admin/keyword-groups/source-groups-show'|| (isset($status) && $status == KeywordGroups::STATUS_ISXODNIE_GRUPPI),
                            ],
                            [
                                'label' => 'В работе',

                                'url' => ['keyword-groups/in-work'],
                                'active' => $this->context->route == 'admin/keyword-groups/in-work' ||
                                    $this->context->route == 'admin/keyword-groups/in-work-show' || (isset($status) && $status ==KeywordGroups::STATUS_V_RABOTE),
                            ],
                            [
                                'label' => 'На модерации',
                                'icon' => 'asterisk',
                                'url' => ['keyword-groups/moderation'],
                                'active' => $this->context->route == 'admin/keyword-groups/moderation' ||
                                    $this->context->route == 'admin/keyword-groups/moderation-show'|| (isset($status) && $status ==KeywordGroups::STATUS_NA_MODERATSII),
                            ],
                            [
                                'label' => 'Обработка групп',

                                'url' => ['keyword-groups/group-processing'],

                                'active' => $this->context->route == 'admin/keyword-groups/group-processing' ||
                                    $this->context->route == 'admin/keyword-groups/group-processing-show'|| (isset($status) && $status ==KeywordGroups::STATUS_OBRABOTKA_GRUPP),
                            ],
                            [
                                'label' => 'Проверка групп',
                                'icon' => 'asterisk',
                                'url' => ['keyword-groups/group-check'],

                                'active' => $this->context->route == 'admin/keyword-groups/group-check' ||
                                    $this->context->route == 'admin/keyword-groups/group-check-show'|| (isset($status) && $status ==KeywordGroups::STATUS_PROVERKA_GRUPP),
                            ],
                            [
                                'label' => 'Готовые группы',

                                'url' => ['keyword-groups/final-group'],

                                'active' => $this->context->route == 'admin/keyword-groups/final-group' ||
                                    $this->context->route == 'admin/keyword-groups/final-group-show' || (isset($status) && $status ==KeywordGroups::STATUS_GOTOVIE_GRUPPI),
                            ],
                            [
                                'label' => 'Использованные',

                                'url' => ['keyword-groups/used-group'],

                                'active' => $this->context->route == 'admin/keyword-groups/used-group' ||
                                    $this->context->route == 'admin/keyword-groups/used-group-show' || (isset($status) && $status ==KeywordGroups::STATUS_ISPOLZOVANNIE),
                            ],

                        ],
                    ],

                    ['label' => 'Отчет', 'icon' => 'book', 'url' => ['report/index']
                    ],

                    ['label' => 'Пользователи', 'icon' => 'user-o', 'url' => ['user/index']],
                    [
                        'label' => 'Настройки',
                        'icon' => 'fa fa-cog',
                        'url' => ['options/index',],

                    ],
                ],
            ]

        ) ?>

    </section>

</aside>

















