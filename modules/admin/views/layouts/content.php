<?php

use dmstr\widgets\Alert;
use yii\widgets\Breadcrumbs;

?>
<div class="content-wrapper">
    <section class="content-header">
        <?=
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
    </section>

    <section class="content">
        <br />
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="row">
        <div class="col-sm-6">Разработка <a href="#">TEO</a>.</div>
        <div class="col-sm-6">Version: <?= Yii::$app->params['version'] ?></div>
    </div>
</footer>

