<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">ТЗ</span><span class="logo-lg">' . Yii::$app->name . '</span>', ['task/index'], ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
				
				<?= Html::a('Перейти на сайт', ['/site/status1?sort=fire'], ['class' => 'hidden-xs', 'target' => '_blank']) ?>
                   
                </li>  
				<li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">                        
                        <span class="hidden-xs"><?= Yii::$app->user->identity->username ?> </span>						
                    </a>                   
                </li>  
				<li class="dropdown user user-menu">
					<?= Html::a(
                                    'Выход <i class="fa fa-sign-out"></i>',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'hidden-xs']
                                ) ?>				
                   
                </li> 					
				
            </ul>
        </div>
    </nav>
</header>











