<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Результаты поиска';
?>

<h1><?= Html::encode('Результаты поиска') ?></h1>

<?php foreach ($tasks as $task): ?>

<p><?= Html::a($task['name'], ["view?id={$task['id']}"]) ?></p>

<?php endforeach; ?>