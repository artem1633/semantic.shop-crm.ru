<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$title = $status ? $title : 'Все ТЗ';

$this->title = $title;
$this->params['breadcrumbs'][] = $title;
?>
<div class="project-index">

    <h1><?= Html::encode($title) ?></h1>

    <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    <?= Html::a('<i class="fa fa-clone"></i> Групповое создание ТЗ', ['group-creation'],
        ['class' => 'btn btn-success']) ?>
    <p></p>
    <div class="panel panel-default panel-body">

        <?php if (Yii::$app->request->get('status') == 3): ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'name',
                        'value' => function ($model) use ($status) {
                            if ($status) {
                                return Html::a(
                                    Html::encode($model->name) . ' (' .
                                    Yii::$app->formatter->asRaw('<span style="color: #333333">'.$model->getTasksCountByStatus($status) . '</span>' )
                                    . ')',
                                    \yii\helpers\Url::to(["status" . $status, 'TaskSearch[project_id]' => $model->id]));
                            } else {
                                return Html::a(
                                    Html::encode($model->name) . ' (' .
                                    Yii::$app->formatter->asRaw('<span style="color: #333333">'.$model->getTasksCountByStatus() . '</span>' )
                                    . ')',
                                    \yii\helpers\Url::to(["status" . $status, 'TaskSearch[project_id]' => $model->id]));
                            }
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 96%;color:red; white-space: nowrap;'],
                    ],
                ],
            ]); ?>
        <?php else: ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'fire',
                        'label' => Html::img('@web/images/fire.png', ['height' => '19px', 'width' => 'auto']),
                        'encodeLabel' => false,
                        'content' => function($model) {
                            if ($model->isFire(Yii::$app->request->get('status')))
                                return Html::img('@web/images/fire.png', ['height' => '20px', 'width' => 'auto']);
                        },
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'headerOptions' => ['style' => 'text-align:center;'],
                    ],
                    [
                        'attribute' => 'name',
                        'value' => function ($model) use ($status) {
                            if ($model->isDeadline(Yii::$app->request->get('status'))){
                                $color = 'red';
                            } else {
                                $color = '';
                            }

                            if ($status) {
                                return Html::a(
                                    Html::encode($model->name) . ' (' .
                                    Yii::$app->formatter->asRaw('<span style="color: #333333">'.$model->getTasksCountByStatus($status) . '</span>' )
                                    . ')',
                                    \yii\helpers\Url::to(["status" . $status, 'TaskSearch[project_id]' => $model->id, 'sort' => 'id']), ['style' => "color:$color"]);
                            } else {
                                return Html::a(
                                    Html::encode($model->name) . ' (' .
                                    Yii::$app->formatter->asRaw('<span style="color: #333333">'.$model->getTasksCountByStatus() . '</span>' )
                                    . ')',
                                    \yii\helpers\Url::to(["status1" . $status, 'TaskSearch[project_id]' => $model->id, 'sort' => 'id']));
                            }
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 96%;color:red; white-space: nowrap;'],


                    ],
                ],
            ]); ?>
        <?php endif; ?>

    </div>
</div>
