<?php

use app\modules\admin\models\Project;
use yii\helpers\Html;
use app\modules\admin\controllers\TaskController;
use app\modules\admin\models\Task;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Task */

$this->title = 'Изменение: ' . $model->name;

$this->params['breadcrumbs'][] = [
    'label' => Task::getStatusesLabel()[$model->status],
    'url' => TaskController::generatePreviousUrl($model->id)
];
$this->params['breadcrumbs'][] = [
    'label' => Project::getProjectNameById($model->project_id),
    'url' => Url::to(['status' . $model->status, 'TaskSearch[project_id]' => $model->project_id])
];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];

$this->params['breadcrumbs'][] = 'Изменение';

?>
<div class="task-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
