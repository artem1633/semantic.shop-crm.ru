<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \app\modules\admin\models\OptionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="panel panel-default panel-body">

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'key',
                    'content' => function($data) {
                        return $data->getKeyAlias($data->key);
                    }
                ],
                'value',

                [
                    'content' => function ($data) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ["update?id={$data->id}"],
                            ['title' => 'Редактирование',]);
                    }
                ],
            ],
        ]);
        ?>

    </div>
</div>














