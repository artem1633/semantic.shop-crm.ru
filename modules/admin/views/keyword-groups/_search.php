<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="keyword-groups-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'semantik') ?>

    <?= $form->field($model, 'frequency') ?>

    <?= $form->field($model, 'add_keywords') ?>

    <?php // echo $form->field($model, 'project') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'import_key') ?>

    <?php // echo $form->field($model, 'in_work') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
