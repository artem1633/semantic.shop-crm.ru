<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\KeywordGroups */

$this->title = 'Изменить группу ключевиков: ' . $model->name;

$url = null;
$project_url = null;
if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_ISXODNIE_GRUPPI){
    $project_url = '/admin/keyword-groups/source-groups-show';
    $url = '/admin/keyword-groups/source-groups';
}

if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_V_RABOTE){
    $url = '/admin/keyword-groups/in-work';
    $project_url = '/admin/keyword-groups/in-work-show';
}

if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_NA_MODERATSII) {
    $url         = '/admin/keyword-groups/moderation';
    $project_url = '/admin/keyword-groups/moderation-show';
}
if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_OBRABOTKA_GRUPP) {
    $url         = '/admin/keyword-groups/group-processing';
    $project_url = '/admin/keyword-groups/group-processing-show';
}

if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_PROVERKA_GRUPP){
    $url         = '/admin/keyword-groups/group-check';
    $project_url = '/admin/keyword-groups/group-check-show';
}

if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_GOTOVIE_GRUPPI){
    $url = '/admin/keyword-groups/final-group';
    $project_url = '/admin/keyword-groups/final-group-show';
}

if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_ISPOLZOVANNIE){
    $url = '/admin/keyword-groups/used-group';
    $project_url = '/admin/keyword-groups/used-group-show';
}


$this->params['breadcrumbs'][] = ['label' =>
    \app\modules\admin\models\KeywordGroups::getStatusName($model->status),'url' =>$url ];
$this->params['breadcrumbs'][] =  ['label' => \app\modules\admin\models\Project::getProjectNameById
($model->project), 'url' => [$project_url, 'project_id' => $model->project]] ;
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="keyword-groups-update">

    <h1><?= Html::encode($this->title) ?> <?= Html::a('Изменить', ['keyword-groups/title','id'=>$model->id], ['class' => 'btn btn-primary']) ?></h1>


    <?= $this->render('_form', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
