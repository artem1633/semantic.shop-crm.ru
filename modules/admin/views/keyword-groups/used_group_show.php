<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => $title, 'url' =>
    ['/admin/keyword-groups/used-group']];
$this->params['breadcrumbs'][] = \app\modules\admin\models\Project::getProjectNameById($_GET['project_id']);
?>
<style>
    hr {
        margin-top: 3px;
        margin-bottom: 3px;
    }
</style>
<div class="keyword-groups-index">

    <h3><?="Проект: " . \app\modules\admin\models\Project::getProjectNameById($_GET['project_id'])
        ?></h3>
    <p>

    </p>
    <?php
    \yii\bootstrap\Modal::begin([
        'header' => 'Группы',
        'toggleButton' => ['label' => '<i class="fa fa-upload"></i> Экспортировать все группы',
            'tag' => 'button',
            'class' => 'btn btn-info',],
    ]);
    ?>
    <?php $form = ActiveForm::begin(['action' => 'used-group']); ?>


    <?php $projects = \app\modules\admin\models\Project::find()->orderBy('name')->all(); ?>

    <?=
    $form->field($model, 'project')->widget(Select2::classname(), [
        'data' => ArrayHelper::Map($projects, 'id', 'name'),
        'options' => ['placeholder' => 'Выберите проект'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <input type="hidden" id="this-project" value=<?=$project_id?>>

    <div class="form-group">
        <span class="btn btn-success" onClick="group()">Экспортировать</span>

    </div>

    <?php ActiveForm::end(); ?>





    <?php \yii\bootstrap\Modal::end(); ?>
    <p>

    </p>
    <div class="panel panel-default panel-body">



        <?php Pjax::begin(['id' => 'pjax_grid']); ?>
        <span class="pull-right"> на странице </span>
        <span class="pull-right"> <?=Html::dropDownList('count_per_page',Yii::$app->session->get('pageSizeUsed',50),
                [20=>'20',30=>'30',50=>'50',100=>'100',200=>'200',500=>'500'],
                ['id'=>'per-page','class'=>'form-control-sm','onChange'=>'set_count_page()'])?> </span>
        <span class="pull-right"> Показывать по </span>
        <?= GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'name' => 'checked',
                        'checkboxOptions' => function($model) {
                            return ['value' => $model->id];
                        },
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'headerOptions' => ['style' => 'text-align:center;']
                    ],
                    [
                        'attribute' => 'name',
                        'content' => function($model){
                            return  Html::a($model->name, ['update', 'id' => $model->id]);
                        },
                        'contentOptions' => function ($model, $key, $index, $column) {
                            return ['style' => 'width:20%;white-space: nowrap;','class'=>
                                ($model->name_change == 0 ? '' : 'a-purple')];
                        },
                        'filter' => false,
                    ],
                    [
                        'attribute' => 'semantik',
                        'value' => function ($model){
                            $user = \app\modules\admin\models\User::findOne(['id' => $model->semantik]);
                            return $user->username;
                        },
                        'filter' => false,
                    ],
                    [
                        'attribute' => 'frequency',
                        'filter' => false,
                    ],
                    [
                        'attribute' => 'keys',
                        'value' => function ($model){

                            return $model->getCountKeywords();

                        },

                        'format' => 'raw',
                        'filter' => true,

                    ],
                    [
                        'attribute' => 'import_key',
                        'value' => function($searchModel){
                            $a = $searchModel->getImportKey();
                            return $a[$searchModel->import_key];
                        },
                        'filterInputOptions' => [
                            'class' => 'form-control',
                            'prompt' => 'Все',
                        ],
                        'filter'=> array(
                            '1' => 'НЧ',
                            '2' => 'ВЧ',
                            '3' => 'На потом',
                        ),

                    ],
                    [
                        'attribute' => 'project',
                        'value' => function($searchModel){
                            $project = \app\modules\admin\models\Project::findOne(['id' => $searchModel->project]);
                            return $project->name;
                        },
                        'filter' => false,

                    ],
                    [
                        'attribute' => 'status',
                        'value' => function($searchModel){
                            $a = \app\modules\admin\models\KeywordGroups::getStatusesLabel();
                            return $a[$searchModel->status];
                        },
                        'filter' => false,

                    ],
                    [
                        'attribute' => 'in_work',
                        'filter' => false,
                    ],

//
//
//                ['class' => 'yii\grid\ActionColumn'],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <div class="counter">Выбрано:&nbsp;<p class="checked">0</p></div>
        <?php Pjax::end(); ?>
    </div>
</div>


<script type="text/javascript">


    function group() {
        $('form').submit(function (e) {
            e.preventDefault();
        });
        var keys = $('#grid').yiiGridView('getSelectedRows');
        var  id = $('#keywordgroups-project').val();
        var  project = $('#this-project').val();
        console.log(project,id);


        var dialog = confirm('Экспортировать?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'to-new-project',
                data: {from: project,id:id},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

    function set_count_page() {


        $.ajax({
            type: "POST",
            url: 'set-count-page',
            data: {pageSizeName: 'pageSizeUsed',pageSize: $('#per-page').val()},
            success: function (result) {
                $.pjax.reload({container: '#pjax_grid'});
            }
        });
    }

</script>


