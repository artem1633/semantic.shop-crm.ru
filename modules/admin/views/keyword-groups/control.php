<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use rmrevin\yii\module\Comments;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->name;
$p = \app\modules\admin\models\Project::findOne(['id' => $model->project]);
$this->params['breadcrumbs'][] = ['label' => 'Группы ключевиков', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $p->name];
$this->params['breadcrumbs'][] = ['label' => $model->getStatusName($model->status)];

?>
<br/>
<div class="keyword-groups-index">


    <p>
    </p>
    <div class="panel panel-default panel-body">
        <h1><?=$model->name;?> </h1>  <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <span class="btn btn-success" onClick="offer()">Предложить выделить</span>
        <span class="btn btn-danger" onClick="deleteKey()"><i class="fa fa-trash"></i> Удалить</span>

        <?php
        \yii\bootstrap\Modal::begin([
            'header' => 'Группы',
            'toggleButton' => ['label' => '<i class="fa fa-upload"></i> Переместить в другую групппу',
                'tag' => 'button',
                'class' => 'btn btn-info',],
        ]);
        ?>
        <div id="changeGroup">
            <?php $form = ActiveForm::begin(['action' => 'group']); ?>


            <?php $projects = \app\modules\admin\models\KeywordGroups::find()->orderBy('name')->all(); ?>
            <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id ])->label(false); ?>
            <?=
            $form->field($model, 'conn_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::Map($projects, 'connect_id', 'name'),
                'options' => ['placeholder' => 'Выберите группу'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>



            <div class="form-group">
                <span class="btn btn-success" onClick="group()">Переместить</span>
                <span class="btn btn-info" onClick="toggleGroup()">Создать новую группу</span>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <div id="newGroup" style="display: none;">
            <?php $form = ActiveForm::begin(['action' => 'group']); ?>


            <?php $projects = \app\modules\admin\models\KeywordGroups::find()->orderBy('name')->all(); ?>
            <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id ])->label(false); ?>
            <?= $form->field($model, 'conn_id')->hiddenInput(['value'=> $model->connect_id ])->label(false);?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true,'value' => '']) ?>



            <div class="form-group">
                <span class="btn btn-success" onClick="newGroup()">Создать</span>
                <span class="btn btn-info" onClick="toggleGroup2()">Выбрать из списка</span>
            </div>

            <?php ActiveForm::end(); ?>
        </div>


        <?php \yii\bootstrap\Modal::end(); ?>


        <p>
        </p>

        <?= GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'rowOptions'=>function($model){
                if($model->color){
                    $code = \app\modules\admin\models\Colors::findOne(['id' => $model->color]);
                    return ['style' => 'background:'.$code->code];
                }
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'name' => 'checked',
                    'checkboxOptions' => function($model) {
                        return ['value' => $model->id];
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'headerOptions' => ['style' => 'text-align:center;']
                ],
                [
                    'attribute' => 'keyword',
                    'filterInputOptions' => [
                        'class'       => 'form-control',
                        'placeholder' => 'Искать ключевик...'
                    ]
                ],
                [
                    'attribute' => 'frequency',
                    'filter' => false,
                ],
            ],
        ]); ?>



    </div>
    <?php $form = ActiveForm::begin(['action' => 'check']); ?>
    <div class="panel panel-default panel-body">


                <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id ])->label(false); ?>
                <?= $form->field($model, 'add_keywords')->textArea(['maxlength' => true, 'rows' => 10])  ?>

    </div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

    </div>

    <?php ActiveForm::end(); ?>
</div>

<?= Comments\widgets\CommentListWidget::widget([
    'entity' => (string)"group_id-{$model->id}", // type and id
]); ?>

<script>
</script>

<script type="text/javascript">

    function offer() {
        $('form').submit(function (e) {
            e.preventDefault();
        });
        var keys = $('#grid').yiiGridView('getSelectedRows');
        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }
        var dialog = confirm('Выделить ?');
        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'offer',
                data: {keylist: keys},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }
    function group() {
        $('form').submit(function (e) {
            e.preventDefault();
        });
        var keys = $('#grid').yiiGridView('getSelectedRows');
        var  id = $('#keywordgroups-conn_id').val();
        var model_id = $('#keywordgroups-id').val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Переместить в новую группу?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'group',
                data: {keylist: keys,id:id,model:model_id},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

    function newGroup() {
        $('form').submit(function (e) {
            e.preventDefault();
        });
        var keys = $('#grid').yiiGridView('getSelectedRows');
        var  id = $('#keywordgroups-conn_id').val();
        var model_id = $('#keywordgroups-id').val();
        var name = $('#keywordgroups-name').val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Создать новую группу?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'new-group',
                data: {keylist: keys,id:id,model:model_id,name:name},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

    function toggleGroup() {
        var x = document.getElementById("changeGroup");
        var b = document.getElementById("newGroup");
        x.style.display = "none";
        b.style.display = "block";

    }

    function toggleGroup2() {
        var x = document.getElementById("changeGroup");
        var b = document.getElementById("newGroup");
        x.style.display = "block";
        b.style.display = "none";
    }

    function deleteKey() {

        var keys = $('#grid').yiiGridView('getSelectedRows');


        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ключевики',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('точно Удалить?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'delete-key',
                data: {keylist: keys},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

</script>


