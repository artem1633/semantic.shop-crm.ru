<?php

use app\modules\admin\models\User;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\models\Comment */
/* @var $keyword_groups_model \app\modules\admin\models\KeywordGroups */

$this->title = 'Исходные группы';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' =>
        ['/admin/keyword-groups/source-groups']
];
$this->params['breadcrumbs'][] = \app\modules\admin\models\Project::getProjectNameById($_GET['project_id']);
?>
<style>
    hr {
        margin-top: 3px;
        margin-bottom: 3px;
    }
</style>
<div class="keyword-groups-index">
    <h3>
        <?= "Проект: " . \app\modules\admin\models\Project::getProjectNameById($_GET['project_id']) ?></h3>
    <span class="btn btn-danger" onClick="to_delete()"><i class="fa fa-close"></i> Удалить выбранные</span>
    <span class="btn btn-info" onClick="to_late()"><i class="fa fa-close"></i> На потом</span>
    <span class="btn btn-success" onClick="designate()"><i class="fa fa-check"></i> Назначить исполнителя</span>

    <p></p>

    <div class="panel panel-default panel-body">
        <?php Pjax::begin(['id' => 'pjax_grid']); ?>
        <span class="pull-right"> на странице </span>
        <span class="pull-right"> <?= Html::dropDownList('count_per_page',
                Yii::$app->session->get('pageSizeSource', 50),
                [20 => '20', 30 => '30', 50 => '50', 100 => '100', 200 => '200', 500 => '500'],
                ['id' => 'per-page', 'class' => 'form-control-sm', 'onChange' => 'set_count_page()']) ?> </span>
        <span class="pull-right"> Показывать по </span>
        <?php
        try {
            echo GridView::widget([
                'id' => 'grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => require(__DIR__ . '/_column_source_group_show.php')
            ]);
        } catch (Exception $e) {
            echo $e->getMessage();
        } ?>
        <div class="counter">Выбрано:&nbsp;<p class="checked">0</p></div>
        <?php Pjax::end(); ?>
    </div>
</div>

<?php
Modal::begin([
    'id' => 'addComment',
    'header' => 'Коментарии',
    'size' => 'modal-lg',

]);
?>
<div class="comments-widget"><h3 class="comment-title">Добавить комментарий</h3>
    <a name="commentcreateform"></a>
    <div class="row comment-form">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'text')->textarea(['maxlength' => true, 'value' => '']) ?>

            <button type="submit" class="btn btn-primary">Добавить комментарий</button>
            <button type="reset" class="btn btn-link">Отмена</button>
        </div>

        <?php ActiveForm::end(); ?>


    </div>
</div>
<?php
Modal::end();
?>

<?php
Modal::begin([
    'id' => 'modal-designate',
    'header' => 'Назначение исполнителя',
    'size' => 'modal-md',

]); ?>
<?php $form = ActiveForm::begin([
    'action' => 'set-designates',
    'id' => 'designate-form'
]); ?>
<?= $form->field($keyword_groups_model, 'semantik')->dropDownList((new User)->getList(), ['prompt' => 'Выберите пользователя'])->label(false) ?>
<?= $form->field($keyword_groups_model, 'ids')->hiddenInput(['id' => 'ids'])->label(false) ?>
<?= Html::submitButton('Назначить', [
    'class' => 'btn btn-primary',
]) ?>
<?= Html::Button('Отмена', [
    'class' => 'btn btn-link',
    'type' => 'reset',
]) ?>
<?php ActiveForm::end(); ?>
<?php Modal::end(); ?>
<?php


$this->registerCss("
.form-control{
padding: 6px 3px !important;
}
");
?>

<script type="text/javascript">


    function addComment(id) {
        var action = $('#w0').attr('action');
        action = action + "&id=" + id;
        $('#w0').attr('action', action);

        $('#addComment').modal('show');
    }

    function to_moderation() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Отправить на доработку?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'to-moderation',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function to_delete() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Удалить?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'delete-selected',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function to_late() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('На потом?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'to-late',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function to_accept() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Принять выбранные группы?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'to-accept',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function set_count_page() {


        $.ajax({
            type: "POST",
            url: 'set-count-page',
            data: {pageSizeName: 'pageSizeSource', pageSize: $('#per-page').val()},
            success: function (result) {
                $.pjax.reload({container: '#pjax_grid'});
            }
        });
    }

    function designate() {
        var moadal_form = $('#modal-designate');
        var grid = $('#grid');

        var keys = grid.yiiGridView('getSelectedRows');
        if (keys.length === 0) {
            swal({
                title: "Ошибка",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "red"
            });
            return;
        }
        // debugger;
        keys.join(',');
        $('#ids').val(keys);
        moadal_form.modal('show');
    }

</script>


