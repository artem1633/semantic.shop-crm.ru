
<?php

use app\modules\admin\models\Project;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $project app\modules\admin\models\Project */

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => $title, 'url' =>
    ['/admin/keyword-groups/final-group']];;
$this->params['breadcrumbs'][] = Project::getProjectNameById($_GET['project_id']);
?>
<style>
    hr {
        margin-top: 3px;
        margin-bottom: 3px;
    }
</style>
<div class="keyword-groups-index">

    <h3><?="Проект: " . Project::getProjectNameById($_GET['project_id'])
        ?></h3>

    <span class="btn btn-danger" onClick="deleteSelected()"><i class="fa fa-trash-o"></i> Удалить</span>
    <span class="btn btn-info" onClick="to_late()"><i class="fa fa-close"></i> На потом</span>
    <input type="hidden" id="project-project" value=<?=$project->id?>>

    <?php if($project->tz_binet == Project::BINET_MULTIPLE):?>
    <?php Modal::begin([
        'header' => 'Проекты в TZ Binet',
        'toggleButton' => ['label' => '<i class="fa fa-check"></i> Подготовить ТЗ',
            'tag' => 'button',
            'class' => 'btn btn-success',],
    ]);
    ?>
        <?php $form = ActiveForm::begin(['action' => 'group']); ?>
        <?php $projects = \app\modules\admin\models\TzBinetConnect::findAll(['project_id' => $project->id]); ?>
        <?=$form->field($project, 'binet')->widget(Select2::classname(), [
            'data' => ArrayHelper::Map($projects, 'name', 'name'),
            'options' => ['placeholder' => 'Выберите проект TZ Binet'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(false);
        ?>
        <div class="form-group">
            <span class="btn btn-success" onClick="select1()">Создать</span>
        </div>
        <?php ActiveForm::end(); ?>
    <?php Modal::end(); ?>
    <?php elseif($project->tz_binet == Project::BINET_ONE):?>
        <?php $id = \app\modules\admin\models\TzBinetConnect::findOne(['project_id' => $project->id]);?>
        <input type="hidden" id="project-tz-binet" value=<?=$id->name?>>
        <span class="btn btn-success" onClick="select2()"><i class="fa fa-check"></i> Подготовить ТЗ</span>
    <?php else:?>
        <?php Modal::begin([
            'header' => 'Проект в TZ Binet',
            'toggleButton' => ['label' => '<i class="fa fa-check"></i> Подготовить ТЗ',
                'tag' => 'button',
                'class' => 'btn btn-success',],
        ]);
        ?>
        <?php $form = ActiveForm::begin(['action' => 'group']); ?>
        <?php $projects = \app\modules\admin\models\KeywordGroups::find()->orderBy('name')->all(); ?>
        <?= $form->field($project, 'tz_binet')->textInput(['maxlength' => true])->label(false) ?>
        <div class="form-group">
            <span class="btn btn-success" onClick="select3()">Создать</span>

        </div>
        <?php ActiveForm::end(); ?>
        <?php Modal::end(); ?>
    <?php endif;?>


    <p>
    </p>
    <div class="panel panel-default panel-body">



        <?php Pjax::begin(['id' => 'pjax_grid']); ?>
        <span class="pull-right"> на странице </span>
        <span class="pull-right"> <?=Html::dropDownList('count_per_page',Yii::$app->session->get('pageSizeFinal',50),
                [20=>'20',30=>'30',50=>'50',100=>'100',200=>'200',500=>'500'],
                ['id'=>'per-page','class'=>'form-control-sm','onChange'=>'set_count_page()'])?> </span>
        <span class="pull-right"> Показывать по </span>


        <?= GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => require(__DIR__.'/_column_final_group_show.php'),
        ]); ?>
        <div class="counter">Выбрано:&nbsp;<p class="checked">0</p></div>

        <?php Pjax::end(); ?>
    </div>
</div>


<script type="text/javascript">


    function deleteSelected() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Удалить выбранные Группы?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'delete-selected',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function to_late() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('На потом?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'to-late',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function select1() {
        $('form').submit(function (e) {
            e.preventDefault();
        });
        var binet = $('#project-binet').val();
        var keys = $('#grid').yiiGridView('getSelectedRows');
        var project = $('#project-project').val();
        console.log(keys);
        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        if(binet == 0){
            swal({
                title: "",
                text: 'Нужно указать проект в TZ Binet',
                confirmButtonColor: "#337ab7"
            });
            return;
        }


        var dialog = confirm('Создать ТЗ?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'check',
                data: {keylist: keys,binet:binet,project:project},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }
    function select2() {

        var binet = $('#project-tz-binet').val();
        var keys = $('#grid').yiiGridView('getSelectedRows');
        var project = $('#project-project').val();
        console.log(keys);
        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Создать ТЗ?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'check',
                data: {keylist: keys,binet:binet,project:project},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

    function select3() {
        $('form').submit(function (e) {
            e.preventDefault();
        });

        var binet = $('#project-tz_binet').val();
        var keys = $('#grid').yiiGridView('getSelectedRows');
        var project = $('#project-project').val();
        var create = "create";

        console.log(keys);
        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        if (binet == '') {
            swal({
                title: "",
                text: 'Нужно указать проект в TZ Binet',
                confirmButtonColor: "#337ab7"
            });
            return;
        }
        var dialog = confirm('Создать ТЗ?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'check',
                data: {keylist: keys,binet:binet,project:project,create:create},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }
    function set_count_page() {


        $.ajax({
            type: "POST",
            url: 'set-count-page',
            data: {pageSizeName: 'pageSizeFinal',pageSize: $('#per-page').val()},
            success: function (result) {
                $.pjax.reload({container: '#pjax_grid'});
            }
        });
    }
    // function addComment(id) {
    //     var action = $('#w0').attr('action');
    //     action = action + "&id="+id;
    //     $('#w0').attr('action',action);
    //
    //     $('#addComment').modal('show');
    // }
</script>


<?php
//Modal::begin([
//    'id' => 'addComment',
//    'header' => 'Коментарии',
//    'size'=>'modal-lg',
//
//]);
//?>
<!--<div class="comments-widget"><h3 class="comment-title">Добавить комментарий</h3>-->
<!--    <a name="commentcreateform"></a>-->
<!--    <div class="row comment-form">-->
<!--        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
<!--            --><?php //$form = ActiveForm::begin(); ?>
<!--            --><?php // echo $form->field($model, 'text')->textarea(['maxlength' => true,'value' => '']) ?>
<!---->
<!--            <button type="submit" class="btn btn-primary">Добавить комментарий</button><button type="reset" class="btn btn-link">Отмена</button>        </div>-->
<!---->
<!--        --><?php //ActiveForm::end(); ?>
<!---->
<!---->
<!--    </div>-->
<!--</div>-->
<!---->
<?php
//Modal::end();
//?>