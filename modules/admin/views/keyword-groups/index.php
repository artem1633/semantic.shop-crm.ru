<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keyword-groups-index">

    <h1><?= Html::encode($title) ?></h1>
    <p>

    </p>
    <div class="panel panel-default panel-body">


        <?php Pjax::begin(['id' => 'pjax_grid']); ?>

        <?php
        try {
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'name' => 'checked',
                        'checkboxOptions' => function ($model) {
                            return ['value' => $model->id];
                        },
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'headerOptions' => ['style' => 'text-align:center;']
                    ],
                    'name',
                    'semantik',
                    'frequency',
                    [
                        'attribute' => 'project',
                        'value' => function ($searchModel) {
                            $project = \app\modules\admin\models\Project::findOne(['id' => $searchModel->project]);
                            return $project->name;
                        },

                    ],
                    [
                        'attribute' => 'status',
                        'value' => function ($searchModel) {
                            $a = \app\modules\admin\models\KeywordGroups::getStatusesLabel();
                            return $a[$searchModel->status];
                        },

                    ],
                    [
                        'attribute' => 'import_key',
                        'value' => function ($searchModel) {
                            $a = $searchModel->getImportKey();
                            return $a[$searchModel->import_key];
                        },
                        'filterInputOptions' => [
                            'class' => 'form-control',
                            'prompt' => 'Все',

                        ],
                        'filter' => array(
                            '1' => 'НЧ',
                            '2' => 'ВЧ',
                            '3' => 'На потом',
                        ),

                    ],
                    'in_work',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
        } catch (Exception $e) {
            echo $e->getMessage();
        } ?>
        <?php Pjax::end(); ?>
    </div>
</div>


<script type="text/javascript">

    function deleteSelectedTasks() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ТЗ',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Удалить выбранные ТЗ?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'delete-selected-tasks',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function setFire() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ТЗ',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Установить пометку «Горит»?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'fire',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function group() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ТЗ',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Объединить выбранные ТЗ в группу?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'group',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function changeDeadline() {

        var keys = $('#grid').yiiGridView('getSelectedRows');
        var from_date = $('#from_date').datepicker().val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ТЗ',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Изменить дедлайны выбранных ТЗ?');

        if (dialog == true) {
            $.ajax({
                type: "POST",
                url: 'change-deadline',
                data: {
                    keylist: keys,
                    deadline: from_date
                },
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }
</script>


