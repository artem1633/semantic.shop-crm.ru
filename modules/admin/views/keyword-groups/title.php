<?php

use yii\helpers\Html;;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\KeywordGroups */

$this->title =  $model->name;
$url = null;
if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_ISXODNIE_GRUPPI)
    $url = '/admin/keyword-groups/source-groups';
if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_V_RABOTE)
    $url = '/admin/keyword-groups/in-work';
if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_NA_MODERATSII)
    $url = '/admin/keyword-groups/moderation';
if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_OBRABOTKA_GRUPP)
    $url = '/admin/keyword-groups/group-processing';
if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_PROVERKA_GRUPP)
    $url = '/admin/keyword-groups/group-check';
if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_GOTOVIE_GRUPPI)
    $url = '/admin/keyword-groups/final-group';
if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_ISPOLZOVANNIE)
    $url = '/admin/keyword-groups/used-group';

$this->params['breadcrumbs'][] = ['label' =>
    \app\modules\admin\models\KeywordGroups::getStatusName($model->status),'url' =>$url ];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="keyword-groups-update">

    <h1><?= Html::encode($this->title) ?></h1>


    <div class="keyword-groups-form">

        <?php $form = ActiveForm::begin(); ?>
        <div class="panel panel-default panel-body">

            <div class="row">

                <div class="col-md-4">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>


            </div>

        </div>
        <div class="form-group">
            <?= Html::submitButton('Изменить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>