<?php
use yii\helpers\Html;
return
    [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'yii\grid\CheckboxColumn',
            'name' => 'checked',
            'checkboxOptions' => function($model) {
                return ['value' => $model->id];
            },
            'contentOptions' => ['style' => 'text-align:center;'],
            'headerOptions' => ['style' => 'text-align:center;']
        ],
        ['attribute' => 'comments',
        'value' => function ($model) {
            return  $model->comments . '<div><a href="#" class="add-comment" onclick="addComment('.$model->id.')">Комментировать</a></div>';
        },
        'format'=>'raw',
        'contentOptions' => ['style' => 'width: 15%'],
    ],
        [
            'attribute' => 'name',
            'content' => function($model){
                return  Html::a($model->name, ['update', 'id' => $model->id]);
            },
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'width:5%','class'=>
                    ($model->name_change == 0 ? '' : 'a-purple')];
            },
            'filter' => false,
        ],
        [
            'attribute' => 'main_keywords',
            'format'=>'raw',
            'content' => function($model){
                return $model->colorKey."+<br/>".nl2br ($model->add_keywords);
            },
            'contentOptions' => ['style' => 'width: 40%;'],
            'filter' => true,
        ],
        [
            'attribute' => 'frequency_keywords',
            'label' => "Частот ключа",
            'content' => function($model){
                return $model->colorFreq;
            },
            'format' => 'raw',
            'filter' => true,
        ],
        [
            'attribute' => 'count_keywords',
            'label' => "Кол-во".'<br />'."ключей",
            'encodeLabel'=>false,
            'contentOptions' => ['style' => 'width: 5%;'],
            'filter' => true,
        ],
        [
            'attribute' => 'semantik',
            'value' => function ($model){
                $user = \app\modules\admin\models\User::findOne(['id' => $model->semantik]);
                return $user->username;
            },
            'filter' => false,
        ],
        [
            'attribute' => 'in_work',
            'filter' => false,
        ],

        ['class' => 'yii\grid\ActionColumn'],
    ]
?>