<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => $title, 'url' =>
    ['/admin/keyword-groups/group-check']];
$this->params['breadcrumbs'][] = \app\modules\admin\models\Project::getProjectNameById($_GET['project_id']);
?>
<style>
    hr {
        margin-top: 3px;
        margin-bottom: 3px;
    }
</style>
<div class="keyword-groups-index">

    <h3><?="Проект: " . \app\modules\admin\models\Project::getProjectNameById($_GET['project_id'])
        ?></h3>

    <span class="btn btn-success" onClick="to_accept()"><i class="fa fa-check"></i> Принять</span>
    <span class="btn btn-info" onClick="to_moderation()"><i class="fa fa-close"></i> На доработку</span>
    <p>

    </p>

    <div class="panel panel-default panel-body">



        <?php Pjax::begin(['id' => 'pjax_grid']); ?>
        <span class="pull-right"> на странице </span>
        <span class="pull-right"> <?=Html::dropDownList('count_per_page',Yii::$app->session->get('pageSizeCheck',50),
                [20=>'20',30=>'30',50=>'50',100=>'100',200=>'200',500=>'500'],
                ['id'=>'per-page','class'=>'form-control-sm','onChange'=>'set_count_page()'])?> </span>
        <span class="pull-right"> Показывать по </span>

        <?= GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => require(__DIR__.'/_column_group_check_show.php'),
        ]); ?>
        <div class="counter">Выбрано:&nbsp;<p class="checked">0</p></div>

        <?php Pjax::end(); ?>
    </div>
</div>


<script type="text/javascript">


    function to_moderation() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Отправить на доработку?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'to-moderation',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function to_accept() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Принять выбранные группы?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'to-accept',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function set_count_page() {


        $.ajax({
            type: "POST",
            url: 'set-count-page',
            data: {pageSizeName:'pageSizeCheck', pageSize: $('#per-page').val()},
            success: function (result) {
                $.pjax.reload({container: '#pjax_grid'});
            }
        });
    }

    function addComment(id) {
        var action = $('#w0').attr('action');
        action = action + "&id="+id;
        $('#w0').attr('action',action);

        $('#addComment').modal('show');
    }
</script>


<?php
Modal::begin([
    'id' => 'addComment',
    'header' => 'Коментарии',
    'size'=>'modal-lg',

]);
?>
    <div class="comments-widget"><h3 class="comment-title">Добавить комментарий</h3>
        <a name="commentcreateform"></a>
        <div class="row comment-form">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'text')->textarea(['maxlength' => true,'value' => '']) ?>

                <button type="submit" class="btn btn-primary">Добавить комментарий</button><button type="reset" class="btn btn-link">Отмена</button>        </div>

            <?php ActiveForm::end(); ?>


        </div>
    </div>

<?php
Modal::end();
?>