<?php
use yii\helpers\Html;
return
[
    ['class' => 'yii\grid\SerialColumn'],
    [
        'class' => 'yii\grid\CheckboxColumn',
        'name' => 'checked',
        'checkboxOptions' => function($model) {
            return ['value' => $model->id];
        },
        'contentOptions' => ['style' => 'text-align:center;'],
        'headerOptions' => ['style' => 'text-align:center;']
    ],
    [
        'attribute' => 'name',
        'content' => function($model){
            return  Html::a($model->name, ['update', 'id' => $model->id]);
        },
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'width:20%;white-space: nowrap;','class'=>($model->name_change == 0 ? '' : 'a-purple')];
        },
        'filter' => false,
    ],
    [
        'attribute' => 'semantik',
        'value' => function ($model){
            $user = \app\modules\admin\models\User::findOne(['id' => $model->semantik]);
            return $user->username;
        },
        'filter' => false,
    ],
    [
        'attribute' => 'frequency',
        'filter' => false,
    ],
//    [
//    'attribute' => 'keys',
//    'value' => function ($model){
//        return $model->getCountKeywords();
//    },
//    'format' => 'raw',
//    'filter' => true,
//    ],
        [
            'attribute' => 'count_keywords',
            'label' => "Кол-во".'<br />'."ключей",
            'encodeLabel'=>false,
            'contentOptions' => ['style' => 'width: 5%;'],
            'filter' => true,
        ],
    [
        'attribute' => 'import_key',
        'value' => function($searchModel){
            $a = $searchModel->getImportKey();
            return $a[$searchModel->import_key];
        },
        'filterInputOptions' => [
            'class' => 'form-control',
            'prompt' => 'Все',
        ],
        'filter'=> array(
            '1' => 'НЧ',
            '2' => 'ВЧ',
            '3' => 'На потом',
            '4' => 'Кроме на потом',
        ),
    ],
//    [
//        'attribute' => 'max_frequency',
//        'label' => "Частот ключа",
//        'content' => function($model){
//            return $model->colorFreq;
//        },
//        'format' => 'raw',
//        'filter' => true,
//    ],

];
?>