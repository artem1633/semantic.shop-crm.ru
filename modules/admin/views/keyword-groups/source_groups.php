<?php

use app\modules\admin\models\KeywordGroups;
use app\modules\admin\models\Project;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
///* @var $model \app\modules\admin\models\Project*/

$title = 'Исходные группы';

$this->title = $title;
$this->params['breadcrumbs'][] = $title;

?>
<div class="project-index">

    <h1><?= Html::encode($title) ?></h1>

    <p></p>
    <div class="panel panel-default panel-body">


        <?php
        try {
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'summary' => false,
                'rowOptions' => function (Project $model) {
                    Yii::info(json_decode(json_encode($model), true), 'test');

                    if (!$model->getGroups(KeywordGroups::STATUS_ISXODNIE_GRUPPI)) {
                        return array('hidden' => 'hidden');
                    }

                },
                'columns' => [

                    [
                        'attribute' => 'name',
                        'value' => function (Project $model) {

                            return Html::a(
                                Html::encode($model->name) . ' (' .
                                Yii::$app->formatter->asRaw('<span style="color: #333333">' . $model->getGroups(KeywordGroups::STATUS_ISXODNIE_GRUPPI) . '</span>')
                                . ')',
                                \yii\helpers\Url::to(["source-groups-show", 'project_id' => $model->id]));

                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 96%;color:red; white-space: nowrap;'],
                    ],
                    [
                        'attribute' => 'count_used',
                        'label' => 'Готовых групп / должно быть (В работе)',
                        'value' => function (Project $model) {

                            return $model->getCountUsedGroups() . " / " . $model->allowed_groups . ' (+' . $model->countUnclearUsedGroups . ')';

                        },
                        'format' => 'raw',
                        'contentOptions' => function (Project $model) {
                            return (int)$model->getCountUsedGroups() < (int)$model->allowed_groups ? ['style' => 'color:red; width: 10%; white-space: nowrap;'] : ['style' => 'width: 10%; white-space: nowrap;'];

                        },


                    ],
                ],
            ]);
        } catch (Exception $e) {
            Yii::info($e->getMessage(), 'error');
            echo $e->getMessage();
        } ?>
    </div>
</div>
