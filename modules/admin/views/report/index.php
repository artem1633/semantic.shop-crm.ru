<?php

use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ReportSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = 'Отчеты';
$this->params['breadcrumbs'][] = $this->title;

?>


<div>
    <?php Pjax::begin(['id' => 'pjax_grid']); ?>

    <?= GridView::widget([
        'id' => 'grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//        [
//            'class' => 'yii\grid\CheckboxColumn',
//            'name' => 'checked',
//            'checkboxOptions' => function ($model) {
//                return ['value' => $model->id];
//            },
//            'contentOptions' => ['style' => 'text-align:center;'],
//            'headerOptions' => ['style' => 'text-align:center;'],
//        ],
//            [
//                'attribute' => 'name',
//                'value' => function ($model) {
//                    return Html::a(Html::encode($model->name) .
//                        \app\modules\admin\widgets\CommentCountWidget::widget(['task' => $model]),
//                        Url::to(["update?id={$model->id}"]));
//                },
//                'format' => 'raw',
//            ],
//            [
//                'attribute' => 'user.username',
//                'label' => 'Пользователь',
//                'content' => function (\app\modules\admin\models\Report $report) {
//                    return $report->user? $report->user->username : 'По всем пользователям';
//
//                },
//            ],
            [
                'attribute' => 'userName',
                'label' => 'Пользователь',
                'content' => function (\app\modules\admin\models\Report $report) {
                    return $report->user->username;
                },
            ],
            [
                'attribute' => 'dateRange',
                'content' => function (\app\modules\admin\models\Report $report) use ($searchModel) {
                    return $searchModel->dateRange;
                },

                'label' => "Период",
                'options' => [
                    'format' => 'YYYY-MM-DD',
                ],
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [
                    'attribute' => 'only_date',
                    'presetDropdown' => true,
                    'convertFormat' => false,
                    'pluginOptions' => [
                        'separator' => ' - ',
                        'format' => 'YYYY-MM-DD',
                        'locale' => [
                            'format' => 'YYYY-MM-DD'
                        ],
                    ],
                    'pluginEvents' => [
                        //"apply.daterangepicker" => "function() { apply_filter('only_date') }",

                        "cancel.daterangepicker" => "function(ev, picker) {
picker.element[0].children[1].textContent = '';
$(picker.element[0].nextElementSibling).val('').trigger('change');
}",
                        'apply.daterangepicker' => 'function(ev, picker) {
var val = picker.startDate.format(picker.locale.format) + picker.locale.separator +
picker.endDate.format(picker.locale.format);

picker.element[0].children[1].textContent = val;
$(picker.element[0].nextElementSibling).val(val);
}'],

                ],
            ],


            [
                'attribute' => 'doneCount',
                'label' => 'Выполнено (ТЗ)',
                'content' => function (\app\modules\admin\models\Report $report) {
                    return $report->doneCount;
                },
            ],
            [
                'attribute' => 'doneCountGroup',
                'label' => 'Выполнено (Группы)',
                'content' => function (\app\modules\admin\models\Report $report) {
                    return $report->doneCountGroup;
                },
            ],

        ],
    ]);
    ?>

    <?php Pjax::end(); ?>

</div>
<div>
    <h1>Итого:</h1>
    <table class="table table-striped">
        <thead class="thead-dark">
        <tr>

            <th scope="col">Всего семантиков</th>
            <th scope="col">Выполнено (ТЗ)</th>
            <th scope="col">Выполнено (Группы)</th>

        </tr>
        </thead>
        <tbody>
        <tr>

            <td id="full-count"></td>
            <td id="full-sum"></td>
            <td id="full-group"></td>

        </tr>

        </tbody>
    </table>
</div>
<?php
$this->registerJs('
$(document).ready(function(){
    
    var sum = 0;
    var count = 0;
    var grp = 0;
    var grp_sum = 0;
    $(\'.table-bordered tbody tr\').each(function() {
        count++;
        var lasttd =  $(this).find(\':nth-last-child(2)\').html();
        var grp =  $(this).find(\':nth-last-child(1)\').html();
        sum += parseInt(lasttd);
        grp_sum += parseInt(grp);
        
    });
    $(\'#full-count\').html(count);
    $(\'#full-sum\').html(sum);
    $(\'#full-group\').html(grp_sum);
   
   
    $(\'#grid\').change(function(){
    location.reload();
    });
});
');
?>

<script type="text/javascript">

    function deleteSelectedTasks() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ТЗ',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Удалить выбранные ТЗ?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'delete-selected-tasks',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function searchTask() {

        var search_text = $("#search_text").val().trim();

        if (search_text == '') {
            swal({
                title: "",
                text: 'Укажите текст для поиска.',
                confirmButtonColor: "#337ab7"
            });

            return;
        }

        window.open('search?text=' + search_text);
    }

    function changeDeadline() {

        var keys = $('#grid').yiiGridView('getSelectedRows');
        var from_date = $('#from_date').datepicker().val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ТЗ',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Изменить дедлайны выбранных ТЗ?');

        if (dialog == true) {
            $.ajax({
                type: "POST",
                url: 'change-deadline',
                data: {
                    keylist: keys,
                    deadline: from_date
                },
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }
</script>










