<?php

namespace app\modules\admin\controllers;

use app\models\Comment;
use app\modules\admin\models\Keywords;
use app\modules\admin\models\Project;
use app\modules\admin\models\ProjectSearch;
use app\modules\admin\models\Task;
use app\modules\admin\models\TzBinetConnect;
use rmrevin\yii\module\Comments\widgets\CommentListWidget;
use Yii;
use app\modules\admin\models\KeywordGroups;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * KeywordGroupsController implements the CRUD actions for KeywordGroups model.
 */
class KeywordGroupsController extends Controller
{
    use MyTrait;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        return $this->actionSourceGroups();
    }

    /**
     * Displays a single KeywordGroups model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionSourceGroups()
    {
//        $status = KeywordGroups::STATUS_ISXODNIE_GRUPPI;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('source_groups', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionSourceGroupsShow()
    {
        $model = new Comment();
        if ($model->load(yii::$app->request->post())) {

            $model->entity = 'group_id-' . $_GET['id'];
            $model->created_by = Yii::$app->user->id;
            $model->updated_by = Yii::$app->user->id;
            $model->created_at = time();
            $model->updated_at = time();
            $model->save();
        }
        $keyword_groups_model = new KeywordGroups();

        $status = KeywordGroups::STATUS_ISXODNIE_GRUPPI;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['keyword_groups.status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $pageSize = Yii::$app->session->get('pageSizeSource', 50);
        $dataProvider->pagination->pageSize = $pageSize;
        return $this->render('source_groups_show', [
            'model' => $model,
            'keyword_groups_model' => $keyword_groups_model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionInWork()
    {

//        $status = KeywordGroups::STATUS_V_RABOTE;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('in_work', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionInWorkShow()
    {
        $id = $_GET['id'] ?? null;

        if ($id) {
            $model = Comment::getNewComment($id);
        } else {
            $model = new Comment();
        }
        $status = KeywordGroups::STATUS_V_RABOTE;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $pageSize = Yii::$app->session->get('pageSizeWork', 50);
        $dataProvider->pagination->pageSize = $pageSize;

        return $this->render('in_work_show', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionModeration()
    {
//        $status = KeywordGroups::STATUS_NA_MODERATSII;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('moderation', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionModerationShow()
    {
        $model = new Comment();
        if ($model->load(yii::$app->request->post())) {

            $model->entity = 'group_id-' . $_GET['id'];
            $model->created_by = Yii::$app->user->id;
            $model->updated_by = Yii::$app->user->id;
            $model->created_at = time();
            $model->updated_at = time();
            $model->save();
        }
        $status = KeywordGroups::STATUS_NA_MODERATSII;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $pageSize = Yii::$app->session->get('pageSizeModeration', 50);
        $dataProvider->pagination->pageSize = $pageSize;

        return $this->render('moderation_show', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionFinalGroupShow()
    {
        $status = KeywordGroups::STATUS_GOTOVIE_GRUPPI;
        $project = Project::findOne(['id' => $_GET['project_id']]);
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $pageSize = Yii::$app->session->get('pageSizeFinal', 50);
        $dataProvider->pagination->pageSize = $pageSize;

        return $this->render('final_group_show', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'project' => $project,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionFinalGroup()
    {

        $status = KeywordGroups::STATUS_GOTOVIE_GRUPPI;
        $searchModel = new ProjectSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('final_group', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionGroupProcessingShow()
    {
        $model = new Comment();
        if ($model->load(yii::$app->request->post())) {

            $model->entity = 'group_id-' . $_GET['id'];
            $model->created_by = Yii::$app->user->id;
            $model->updated_by = Yii::$app->user->id;
            $model->created_at = time();
            $model->updated_at = time();
            $model->save();
        }
        $status = KeywordGroups::STATUS_OBRABOTKA_GRUPP;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $pageSize = Yii::$app->session->get('pageSizeProcessing', 50);
        $dataProvider->pagination->pageSize = $pageSize;

        return $this->render('group_processing_show', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionGroupProcessing()
    {

        $status = KeywordGroups::STATUS_OBRABOTKA_GRUPP;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('group_processing', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionUsedGroupShow()
    {

        $status = KeywordGroups::STATUS_ISPOLZOVANNIE;
        $model = new KeywordGroups();
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $pageSize = Yii::$app->session->get('pageSizeUsed', 50);
        $dataProvider->pagination->pageSize = $pageSize;

        return $this->render('used_group_show', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'project_id' => $_GET['project_id'],
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionUsedGroup()
    {

        $status = KeywordGroups::STATUS_ISPOLZOVANNIE;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('used_group', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionGroupCheckShow()
    {
        $model = new Comment();
        if ($model->load(yii::$app->request->post())) {

            $model->entity = 'group_id-' . $_GET['id'];
            $model->created_by = Yii::$app->user->id;
            $model->updated_by = Yii::$app->user->id;
            $model->created_at = time();
            $model->updated_at = time();
            $model->save();
        }
        $status = KeywordGroups::STATUS_PROVERKA_GRUPP;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $pageSize = Yii::$app->session->get('pageSizeCheck', 50);
        $dataProvider->pagination->pageSize = $pageSize;

        return $this->render('group_check_show', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionGroupCheck()
    {

        $status = KeywordGroups::STATUS_PROVERKA_GRUPP;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('group_check', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionToNewProject()
    {


        $from_id = $_POST['from'];
        $groups = KeywordGroups::find()->where(['project' => $from_id])->andWhere(['status' => KeywordGroups::STATUS_ISPOLZOVANNIE])->all();
        foreach ($groups as $group) {
            $clone = new KeywordGroups;
            $clone->setAttributes($group->attributes);
            $clone->project = $_POST['id'];
            $clone->connect_id = $group->connect_id;
            $clone->color = $group->color;
            $clone->add_keywords = $group->add_keywords;
            $clone->status = KeywordGroups::STATUS_GOTOVIE_GRUPPI;
            $clone->save();
        }
        Yii::$app->getSession()->setFlash('success', 'Экспортирование завершено успешно!');
        return true;
    }

    /**
     * Creates a new KeywordGroups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KeywordGroups();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing KeywordGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $keywords = Keywords::findAll(['group_id' => $model->connect_id]);
        $searchModel = new KeywordsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['group_id' => $model->connect_id]);
        $dataProvider->query->andWhere(['status' => null]);
        $dataProvider->pagination = false;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $string = Yii::$app->request->post('KeywordGroups')['add_keywords'];
            $string = preg_replace("#\s*?\r?\n\s*?(?=\r\n|\n)#s", "", $string);
            $model->add_keywords = $string;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'keywords' => $keywords,
        ]);
    }

    /**
     * Deletes an existing KeywordGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {

        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('success', 'Группа удалена!');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the KeywordGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KeywordGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KeywordGroups::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionControl($id)
    {
        $group = KeywordGroups::findOne(['id' => $id]);
        $keywords = Keywords::findAll(['group_id' => $group->connect_id]);
        $searchModel = new KeywordsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['group_id' => $group->connect_id]);
        return $this->render('control', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'keywords' => $keywords,
            'model' => $group,
        ]);


    }

    public function actionCheck()
    {

        if (Yii::$app->request->post('create')) {
            $m = new TzBinetConnect();
            $m->project_id = Yii::$app->request->post('project');
            $m->name = Yii::$app->request->post('binet');
            $m->save();

            $project = Project::findOne(['id' => Yii::$app->request->post('project')]);
            $project->tz_binet = Project::BINET_ONE;
            $project->save(false);
        }


        foreach (Yii::$app->request->post('keylist') as $key) {
            $model = KeywordGroups::findOne(['id' => $key]);
            $task = new Task();
            $task->status = Task::STATUS_SOURCE_KEYS;
            $task->project_id = $model->project;
            $task->project_TZBinet = $_POST['binet'];
            $task->name = $model->name;
            $task->fire = 0;
            $task->deadline = Date('Y-m-d', strtotime("+4 days"));
            $task->date_add = date('Y-m-d H:i:s');

            $keys = Keywords::findAll(['group_id' => $model->connect_id]);
//            $with_out_space = "";
//            $with_space = " ";
            $str = $model->frequency . "\n";
            $count = 0;
            $fre = 0;
            foreach ($keys as $k) {
                $count++;

                if ($k->frequency >= 100) {
                    $fre += $k->frequency;
                    if ($count <= 5) {
                        $str .= "  " . $k->keyword . "\n";
                    } else {
                        $str .= $k->keyword . "\n";
                    }
//                    $with_space     = $with_space  . " " .$k->keyword."\n";
//                    $with_out_space = $with_out_space.$k->keyword."\n";

                } else {
                    $str = $str . $k->keyword . "\n";
                }
            }
            $task->main_keywords = rtrim($str, "\n");

//            if($fre >= 100){
//                $task->main_keywords = $with_space . $str;
//            }else{
//                $task->main_keywords = $with_out_space . $str;
//            }
            $task->additional_keywords = '---' . $model->name . "\n" . $model->add_keywords;
            $task->group_id = $model->connect_id;
            $task->import_key = $model->import_key;
            $task->user_id = $model->semantik;
            $model->status = KeywordGroups::STATUS_ISPOLZOVANNIE;
            $model->save(false);
            $task->save(false);
        }

        return 'Группы в статусе Использована';


    }

    public function actionToLate()
    {

        foreach ($_POST['keylist'] as $key) {
            $model = KeywordGroups::findOne(['id' => $key]);
            $model->import_key = 3;
            $model->save();
        }

        return 'Группе присвоин метка «На потом»!';
    }

    public function actionAddComment()
    {
        $request = Yii::$app->request;

        if ($request->isAjax) {
            $id = $_POST['id'];
            $entity = "group_id-" . $id;
            try {
                $comment = CommentListWidget::widget(['entity' => $entity]);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'data' => $comment,
            ];
        }
    }

    /**
     * @return string
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteSelected()
    {
        foreach ($_POST['keylist'] as $key) {

            $model = KeywordGroups::findOne(['id' => $key]);
            $keys = Keywords::findAll(['group_id' => $model->connect_id]);

            foreach ($keys as $k) {
                $k->status = 0;
                $k->group_id = 0;
                $k->save();

            }

            $model->delete();

        }
        return 'Группы удалены!';
    }

    public function actionToIsxodnie()
    {
        foreach ($_POST['keylist'] as $key) {
            $model = KeywordGroups::findOne(['id' => $key]);
            $model->status--;
            if ($model->status < 1) {
                $model->status = 1;
            }
            $model->save();
        }

        return 'Смена статуса завершена';
    }

    public function actionToModeration()
    {
        if (isset($_POST['model_id'])) {
            $model = KeywordGroups::findOne(['id' => $_POST['model_id']]);
            $model->status--;
            if ($model->status < 1) {
                $model->status = 1;
            }
            $model->save();
            Yii::$app->getSession()->setFlash('success', 'Группа отправлень на доработку!');
            return $this->redirect('moderation');
        }

        foreach ($_POST['keylist'] as $key) {
            $model = KeywordGroups::findOne(['id' => $key]);
            $model->status--;
            if ($model->status < 1) {
                $model->status = 1;
            }
            $model->save();
        }

        return 'Группы отправлены на модерации!';
    }

    public function actionToAccept()
    {

        foreach ($_POST['keylist'] as $key) {
            $model = KeywordGroups::findOne(['id' => $key]);
            $model->status++;
            if ($model->status == KeywordGroups::STATUS_GOTOVIE_GRUPPI) {
                $model->accept_time = date('Y-m-d H:i:s');
            }
            $model->save();
        }

        return 'Выбранные группы приняты!';
    }

    public function actionSetCountPage()
    {
        Yii::$app->session->set($_POST['pageSizeName'], $_POST['pageSize']);
        return 'Выбранные группы приняты!';
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionTitle($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['keyword-groups/update', 'id' => $model->id]);
        }

        return $this->render('title', [
            'model' => $model,
        ]);
    }

    public function actionNewGroup()
    {

        $newGroup = new KeywordGroups();
        $count = 0;
        if ($_POST) {
            $group_id = (new \yii\db\Query())
                ->select(['max(connect_id)'])
                ->from('keyword_groups')
                ->scalar();

            $color_count = (new \yii\db\Query())
                ->select(['color'])
                ->from('keyword_groups')
                ->orderBy(['id' => SORT_DESC])
                ->limit(1)
                ->scalar();

            $group_id += 11;
            $color_count++;
            if ($color_count <= 10) {
                $newGroup->color = $color_count;
            } else {
                $newGroup->color = 1;
            }

            $old_group = KeywordGroups::findOne(['id' => $_POST['model']]);
            $newGroup->name = $_POST['name'];
            $newGroup->status = $old_group->status;

            $newGroup->semantik = Yii::$app->user->getId();
            $newGroup->connect_id = $group_id;
            $newGroup->save();

            foreach ($_POST['keylist'] as $key) {
                $count++;
                $model = Keywords::findOne(['id' => (int)$key]);
                $model->group_id = $group_id;


                $old_group->frequency -= $model->frequency;
                $old_group->save();

                if ($count == 1) {

                    if ((int)$model->frequency > 300) {
                        $newGroup->import_key = 2;
                    }

                    if ((int)$model->frequency <= 300) {
                        $newGroup->import_key = 1;
                    }
                }

                $newGroup->project = $old_group->project;
                $newGroup->frequency += $model->frequency;
                $newGroup->save();
                $model->save();
            }
            Yii::$app->getSession()->setFlash('success', 'Создано новая группа');
            return true;
        }

        Yii::$app->getSession()->setFlash('error', 'Ошибка при создание новой группы');
        return false;

    }

    public function actionOffer()
    {
        $use = 1;
        $keywds = Keywords::find()->where(['group_id' => $_POST['id']])->all();
        foreach ($keywds as $keys) {
            if ($keys->color != 0 && $keys->color > $use) {
                $use = $keys->color;
            }
        }
        $use++;
        if ($use == 10) {
            $use = 1;
        }
        foreach ($_POST['keylist'] as $key) {
            $model = Keywords::findOne(['id' => $key]);
//            $group = KeywordGroupsSearch::findOne(['connect_id' => $model->group_id]);
            $model->color = $use;
            $model->save();
        }
        Yii::$app->getSession()->setFlash('success', 'Успешно');
        return true;
    }

    public function actionResetSuggestion()
    {
        $keyword_groups_id = Yii::$app->request->post('keylist');

        $keywords = Keywords::find()
            ->joinWith('keywordGroup kg')
            ->andWhere(['>', 'keywords.color', 0])
            ->andWhere(['kg.id' => $keyword_groups_id])
            ->all();

        Yii::info($keywords, 'test');
        $errors = false;
        foreach ($keywords as $key) {
            $key->color = 0;
            if (!$key->save()) {
                $errors = true;
            }
        }

        if (!$errors) {
            Yii::$app->getSession()->setFlash('success', 'Успешно');
            return 'Операция выполнена успешно.';
        } else {
            Yii::$app->getSession()->setFlash('warning', 'Имеются ошибки');
            return 'Операция выполнена с ошибками.';
        }

    }

    /**
     * Назначает ответственного выбранным исходным группам
     * @throws \HttpRequestException
     */
    public function actionSetDesignates()
    {
        $request = Yii::$app->request;

        if (!$request->isPost) {
            throw new \HttpRequestException('Неверный тип запроса');
        }

        $executor_id = $request->post('KeywordGroups')['semantik'];
        $keyword_groups = explode(',', $request->post('KeywordGroups')['ids']);

        Yii::info('Исполнитель: ' . $executor_id, 'test');
        Yii::info($keyword_groups, 'test');

        /** @var int $id */
        foreach ($keyword_groups as $id) {
            $model = KeywordGroups::findOne($id);
            $model->semantik = $executor_id;
            $model->status = KeywordGroups::STATUS_V_RABOTE;
            $model->in_work = date('Y-m-d H:i:s', time());
            if (!$model->save()) {
                Yii::error($model->errors, 'error');
            }
        }

        if (isset($model)) {
            return $this->redirect(['source-groups-show', 'project_id' => $model->project]);
        }
        return $this->redirect(['source-groups-show']);

    }

    public function actionSetIshodnie()
    {
        foreach ($_POST['keylist'] as $key) {
            $model = KeywordGroups::findOne(['id' => $key]);
            $model->status = KeywordGroups::STATUS_ISXODNIE_GRUPPI;
            if (!$model->save()){
                Yii::error($model->errors, 'error');
            }
        }

        return 'Смена статуса завершена';
    }
}
