<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\KeywordGroups;
use Yii;
use yii\web\Controller;
use app\modules\admin\models\StatAdminReport;

/**
 * Default controller for the `admin` module
 */
class TreatmentController extends Controller {

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        $start = microtime(true);
        $maxGroup = 300;
        $group = KeywordGroups::find()->limit($maxGroup)->where(['max_frequency'=> null])->all();
        $countGroup = 0;
        foreach ($group as $g){
            $text = $g->frequency_keywords; 
            $pos = strpos($text, "\n"); 
            if (!empty($pos)) {
            $line = trim(substr($text, 0, $pos));
            } else {
                $line = $text; 
            }
            $g->max_frequency = (int) $line;
            $g->save();
            $countGroup++;
        }
        $time = microtime(true) - $start;
        if ($countGroup>0) {
            echo "<meta http-equiv=\"refresh\" content=\"5\"/>";
        } else {
            echo 'Обработка завершена';
        }

        echo 'Обработано '.$countGroup.' записей</br>';
        echo 'Время '.$time.' сек</br>';
        echo 'Осталось не обработаних '.KeywordGroups::find()->where(['max_frequency'=> null])->count().' записей</br>';
    }
	
}

