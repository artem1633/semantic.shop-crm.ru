<?php

namespace app\modules\admin\controllers;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\KeywordGroups;

/**
 * KeywordGroupsSearch represents the model behind the search form of `app\modules\admin\models\KeywordGroups`.
 */
class KeywordGroupsSearch extends KeywordGroups
{
    
    public $keysfrequency;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'semantik', 'frequency', 'project', 'status', 'import_key','count_keywords','max_frequency'], 'integer'],
            [['name', 'add_keywords', 'main_keywords', 'in_work','frequency_keywords'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KeywordGroups::find();//->with('keywords');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

//        Yii::info($params, 'test');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'semantik' => $this->semantik,
            'project' => $this->project,
            'status' => $this->status,
            'in_work' => $this->in_work,
        ]);
        if($this->import_key === '4'){
            $query->andFilterWhere(['<>', 'import_key', '3']);
        }elseif($this->import_key === '5'){
            $query->andFilterWhere(['!=', 'import_key', '5']);
        }else{
            $query->andFilterWhere(['import_key' => $this->import_key]);
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'add_keywords', $this->add_keywords])
                ->andFilterWhere(['>=','count_keywords', $this->count_keywords])
                ->andFilterWhere(['>=','frequency', $this->frequency])
                ->andFilterWhere(['like', 'main_keywords', $this->main_keywords])
                ->andFilterWhere(['>=', 'max_frequency', $this->max_frequency]);
            
        return $dataProvider;
    }
}
