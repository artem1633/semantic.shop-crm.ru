<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "tz_binet_connect".
 *
 * @property int $id
 * @property int $project_id
 * @property string $name
 */
class TzBinetConnect extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tz_binet_connect';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id'], 'integer'],
            [['name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Ид',
            'project_id' => 'Project ID',
            'name' => 'Название',
        ];
    }
}
