<?php

namespace app\modules\admin\models\creation;


class SpecialCharacter
{
    const PROJECT_SITE = 'Проект_Сайт:';
    const PROJECT_BINET = 'Проект_Binet:';

    const TWO_SPACE_IN_BEGIN = '--two--space--in--begin--';
    const SPACE = '--space--';
    const BANG = '--!--';

    const TVCH = 'ВЧ';
    const TDX = 'DX';

    public static function getBadWords()
    {
        return [
            'DX',
            'ВЧ',
            'CЧ',
            'НЧ'
        ];
    }

    /**
     * @param $strings
     * @return mixed
     */
    public static function restoreSpecial($strings)
    {
        if (is_array($strings)) {
            foreach ($strings as $key => &$string) {
                $string = str_replace(SpecialCharacter::TWO_SPACE_IN_BEGIN, '  ', $string);
            }
            return $strings;
        } else {
            return str_replace(SpecialCharacter::TWO_SPACE_IN_BEGIN, '  ', $strings);
        }


    }

    /**
     * @param $strings
     * @return mixed
     */
    public static function replaceSpecial($strings)
    {
        if (is_array($strings)) {
            foreach ($strings as &$string) {
                $string = preg_replace('/^(\s{2})/',
                    SpecialCharacter::TWO_SPACE_IN_BEGIN, $string);
                $string = trim($string);
            }
            return $strings;
        } else {
            return preg_replace('/^(\s{2})/',
                SpecialCharacter::TWO_SPACE_IN_BEGIN, $strings);
        }
    }
}