<?php

namespace app\modules\admin\models\creation;


use app\modules\admin\models\Project;

class FakeTaskDataArrayBuilder extends TaskDataArrayBuilder
{
    const PROJECT_ID = 42;

    /**
     * @param $name
     * @return Project
     * @throws TaskDataException
     */
    public function getFindProject($name)
    {
        $p = new Project();
        $p->id = self::PROJECT_ID;
        return $p;
    }

    public function getSimilarTaskCount($name, $project)
    {
        return 0;
    }
}