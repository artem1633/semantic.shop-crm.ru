<?php

namespace app\modules\admin\models;

use yii\base\Model;

/**
 * Class Report
 * @package app\modules\admin\models
 *
 * @property-read int $doneCount
 */
class Report extends Model
{
    /**
     * @var User
     */
    public $user = false;

    /**
     * @var Period
     */
    public $period = false;

    private $_doneCount = null;

    private $_doneCountGroup = null;

    /**
     * @return int
     */
    public function getDoneCount()
    {
        if (is_null($this->_doneCount)) {
            $this->_doneCount = $this->getDoneCountFromDB();
        }

        return $this->_doneCount;
    }

    /**
     * @return int
     */
    public function getDoneCountGroup()
    {
        if (is_null($this->_doneCountGroup)) {
            $this->_doneCountGroup = $this->getDoneCountGroupFromDB();
        }

        return $this->_doneCountGroup;
    }

    /**
     * @return int
     */
    protected function getDoneCountFromDB()
    {
        $query = Task::find()
            ->where([
                'status' => Task::STATUS_USED,
            ]);

        if ($this->user) {
            $query->andWhere([
                'user_id' => $this->user->id
            ]);
        }

        if ($this->period) {
            $query->andWhere([
                'between', 'date_add', $this->period->start, $this->period->end
            ]);
        }

        return intval($query->count());
    }

    /**
     * @return int
     */
    protected function getDoneCountGroupFromDB()
    {


        $query = KeywordGroups::find()
            ->where([
                'status' => KeywordGroups::STATUS_ISPOLZOVANNIE,
            ]);

        if ($this->user) {
            $query->andWhere([
                'semantik' => $this->user->id
            ]);
        }

        if ($this->period) {
            $query->andWhere([
                'between', 'accept_time', $this->period->start, $this->period->end
            ]);
        }

        return intval($query->count());
    }
}



