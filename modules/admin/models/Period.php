<?php

namespace app\modules\admin\models;

use yii\base\Model;


class Period extends Model
{
    public $start;
    public $end;

    /**
     * @param $dataRangeText
     * @return Period
     */
    public static function fromDataRange($dataRangeText)
    {
        $pattern = '/(\d{4}-\d{2}-\d{2}) - (\d{4}-\d{2}-\d{2})/m';

        preg_match_all($pattern, $dataRangeText, $matches, PREG_SET_ORDER, 0);

        return new self([
            'start' => $matches[0][1],
            'end' => $matches[0][2],
        ]);
    }
}



