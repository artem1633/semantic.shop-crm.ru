<?php

namespace app\modules\admin\models;

/**
 * Class Report
 * @package app\modules\admin\models
 *
 * @property-read int $doneCount
 */
class ReportSearch extends Report
{
    public $userName;
    public $dateRange;

    public function rules()
    {
        return [
            [['userName', 'dateRange'], 'safe'],
        ];
    }

    public function search($params)
    {
        $this->load($params);

        if (strlen($this->dateRange)) {
            $period = Period::fromDataRange($this->dateRange);
        } else {
            $period = false;
        }


        $users = User::find()
            ->where([
                'role' => User::ROLE_SEMANTIC,
            ])
            ->all();
        $data = [];
        foreach ($users as $user) {
            $data[] = new \app\modules\admin\models\ReportSearch([
                'user' => $user,
                'period' => $period,
            ]);
        }

        if ($this->userName) {
            $data = array_values(array_filter($data, function (ReportSearch $report) {
                return strpos($report->user->username, $this->userName) !== false;
            }));
        }


        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $data,
//        'pagination' => [
//            'pageSize' => 10,
//        ],

            'sort' => [
                'attributes' => ['user.username', 'doneCount', 'userName','doneCountGroup'],
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

//        // grid filtering conditions
//        $query->andFilterWhere([
//            'status' => $this->statusParam,
//            'project_id' => $this->project_id,
//            'group_id' => $this->group_id,
//            'user_id' => $this->user_id,
//        ]);
//
//        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

}



