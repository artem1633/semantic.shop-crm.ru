<?php

namespace app\modules\admin\models;

use app\models\Comment;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $name
 * @property string $status
 *
 * @property int $user_id
 * @property User $user
 *
 * @property int $project_id
 * @property Project $project
 *
 * @property string $project_TZBinet
 * @property string $date_add
 * @property string $deadline
 * @property string $main_keywords
 * @property string $additional_keywords
 * @property string $task_text
 * @property string $import_key
 * @property int $group_id
 * @property int $fire
 */
class Task extends ActiveRecord
{

    /** @var integer */
    public $num_check;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'project_id', 'project_TZBinet', 'main_keywords'], 'required'],
            [['project_id', 'group_id', 'user_id'], 'integer'],
            [['date_add', 'group_id', 'import_key'], 'safe'],
            ['fire', 'default', 'value' => 0],
            [['name', 'status', 'project_TZBinet'], 'string', 'max' => 255],
            [['main_keywords', 'additional_keywords'], 'string', 'max' => 20000],
            [['task_text'], 'string', 'max' => 50000],
            [
                ['deadline'],
                'filter',
                'filter' => function ($value) {
                    if ($value == '') {
                        return null;
                    }
                    return date('Y-m-d', strtotime($value));
                }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'status' => 'Статус',
            'statusName' => 'Статус',
            'project_id' => 'Проект',
            'projectName' => 'Проект',
            'project_TZBinet' => 'Проект в TZBinet',
            'semantic' => 'Семантик',
            'date_add' => 'Дата создания',
            'deadline' => 'Дедлайн',
            'main_keywords' => 'Основные ключевики',
            'additional_keywords' => 'Дополнительные ключевики',
            'task_text' => 'ТЗ для автора',
            'fire' => 'Горит',
            'keywords' => 'Ключевики',
            'group_id' => 'Группа',
            'groupName' => 'Группа',
            'import_key' => 'Частотность импортируемых ключей',
        ];
    }

    /**
     * @deprecated
     * @return string
     */
    public function getCommentsCount()
    {
        return ' (<i class="fa fa-comments"></i> ' . $this->getCommentsCountAsInteger() . ')';
    }

    public function getCommentsCountAsInteger()
    {
        return Comment::find()
            ->where(['entity' => 'task-' . $this->id])
            ->count();
    }

    public function getKeywords()
    {

        return (trim($this->main_keywords) . PHP_EOL . '!' . PHP_EOL . trim($this->additional_keywords));

    }

    public function getDateAddFormat()
    {

        return date('d.m.Y H:i:s', strtotime($this->date_add));

    }

    public function getDeadlineFormat()
    {

        return date('d.m.Y', strtotime($this->deadline));

    }

    const STATUS_SOURCE_KEYS = '1';
    const STATUS_DONE = '2';
    const STATUS_USED = '3';
    const STATUS_IN_WORK = '4';

    public function getStatuses()
    {
        return self::getStatusesLabel();
    }

    public static function getStatusesLabel()
    {
        return [
            self::STATUS_SOURCE_KEYS => 'Исходные ключевики',
            self::STATUS_DONE => 'Готовое ТЗ',
            self::STATUS_USED => 'Использованные ТЗ',
            self::STATUS_IN_WORK => 'В работе',
        ];
    }

    public function getImportKey()
    {
        return array(
            '1' => 'НЧ',
            '2' => 'СЧ',
            '3' => 'ВЧ'
        );
    }

    public function getStatusName()
    {
        return $this->getStatuses()[$this->status];
    }

    // project 	
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    public function getProjectName()
    {
        if (isset($this->project)) {
            return $this->project->name;
        } else {
            return '';
        }
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($insert) {
            $this->date_add = date('Y-m-d H:i:s');
        }

        if (Yii::$app->request->resolve()[0] == 'admin/task/group-creation') {
            if ($this->isNewRecord && !$this->deadline) {
                $this->deadline = date('Y-m-d', strtotime("+3 days"));
            }

            if ($this->deadline && !$this->isNewRecord) {
                if (!stripos($this->deadline, '-')) {
                    $this->deadline = date_create_from_format('d.m.Y', $this->deadline)->format('Y-m-d');
                }
            }

//                 установим название
            $strings = explode(PHP_EOL, $this->main_keywords);

            $name = '';
            if (isset($strings[0]) && !is_numeric(trim($strings[0]))) {
                $name = trim($strings[0]);
            } elseif (isset($strings[1]) && !is_numeric(trim($strings[1]))) {
                $name = trim($strings[1]);
            }

            $this->name = $name;

            $error = $this->name_unique_project();

            if ($error) {
                $this->addError('main_keywords', $error);

                return false;
            }
        }

        return true;
    }

    public function getMissedKeywords($percentage = false)
    {
        $mainKeywords = $this->replaceChars($this->main_keywords);
        $additionalKeywords = $this->replaceChars($this->additional_keywords);
        $authorText = $this->replaceChars($this->task_text);

        $mainKeywords = str_replace(PHP_EOL, ' ', $mainKeywords);
        $mainKeywords = explode(" ", $mainKeywords);

        foreach (array_keys($mainKeywords) as $key) {
            $mainKeywords[$key] = trim($mainKeywords[$key]);
        }
        $mainKeywords = array_unique($mainKeywords);

        $additionalKeywords = str_replace(PHP_EOL, ' ', $additionalKeywords);
        $additionalKeywords = explode(" ", $additionalKeywords);

        foreach (array_keys($additionalKeywords) as $key) {
            $additionalKeywords[$key] = trim($additionalKeywords[$key]);
        }
        $additionalKeywords = array_unique($additionalKeywords);

        $allKeywords = array_unique(array_merge($mainKeywords, $additionalKeywords));


        $authorText = str_replace(PHP_EOL, ' ', $authorText);
        $authorText = explode(" ", $authorText);

        foreach (array_keys($authorText) as $key) {
            $authorText[$key] = trim($authorText[$key]);
        }
        $authorText = array_unique($authorText);

        //$array1 = array_diff($mainKeywords, $authorText);
        //$array2 = array_diff($additionalKeywords, $authorText);

        $result = array_diff($allKeywords, $authorText);

        if ($percentage) {
            $count = count($result) / count($allKeywords);

            return $count > 0.5 ? false : true;
        }


        return implode("\n", $result);
    }

    public function name_unique_project()
    {
        $keywords = $this->main_keywords;

        $strings = explode(PHP_EOL, $keywords);

        $name = '';

        if (isset($strings[0]) && !is_numeric(trim($strings[0]))) {
            $name = trim($strings[0]);
        } elseif (isset($strings[1]) && !is_numeric(trim($strings[1]))) {
            $name = trim($strings[1]);
        }

        $similarTask = Task::find()
            ->where(['name' => $name])
            ->andWhere(['project_id' => $this->project->id])
            ->andWhere(['<>', 'id', $this->id])
            ->all();

        if (count($similarTask) > 0) {
            return "Ключевое слово '$name' уже используется в другом тз данного проекта.";
        }

        return null;
    }

    public function replaceChars($text)
    {
        return preg_replace("/[^\w\s\d]+/u", " ", $text);
    }

    public function getSemantic()
    {
        $userId = (new Query())
            ->select('task.user_id')
            ->from('task')
            ->where(['id' => $this->id])->one()['user_id'];

        return (new Query())
            ->select('user.username')
            ->from('user')
            ->where(['id' => $userId])
            ->one()['username'];
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getUserNameByID($id)
    {
        return User::find()->where(['id' => $id])->one()['username'];
    }


    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @param Task $task
     * @return Task
     */
    public static function copy(Task $task)
    {
        $newTask = new self();
        $newTask->name = $task->name;
        $newTask->project_TZBinet = $task->project_TZBinet;
        $newTask->date_add = date('Y-m-d');
        $newTask->deadline = date('Y-m-d', strtotime("+3 days"));
        $newTask->task_text = $task->task_text;
        $newTask->main_keywords = $task->main_keywords;
        $newTask->additional_keywords = $task->additional_keywords;
        $newTask->import_key = $task->import_key;
        return $newTask;
    }

    public function getEquipmentList($object_id)
    {
        $datas = TzBinetConnect::find()->where(['project_id' => $object_id])->all();

        return ArrayHelper::map($datas, 'name', 'name');
    }
}





