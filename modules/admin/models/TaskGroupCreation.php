<?php

namespace app\modules\admin\models;

use app\modules\admin\models\creation\TaskBuilder;
use app\modules\admin\models\creation\TaskDataArrayBuilder;
use app\modules\admin\models\creation\TaskDataException;

class TaskGroupCreation extends \yii\base\Model
{
    public $project_id;
    public $project_TZBinet;
    public $keywords;

    public function attributeLabels()
    {
        return [
            'project_id' => 'Проект',
            'project_TZBinet' => 'Проект в TZBinet',
            'keywords' => 'Ключевики',
        ];
    }

    public function rules()
    {
        return [
            [['keywords'], 'required'],
            [['project_id'], 'integer'],
            [['project_TZBinet'], 'string', 'max' => 255],
        ];
    }

    public function CreateMany()
    {
        if (!$this->validate()) {
            return null;
        }

        try {

            $taskDataArrayBuilder = new TaskDataArrayBuilder(['keywords' => $this->keywords]);
            $tasksData = $taskDataArrayBuilder->build();
            foreach ($tasksData as $taskData) {
                if (empty(trim($taskData))) {
                    continue;
                }

                $taskBuilder = new TaskBuilder([
                    'projectId' => $taskDataArrayBuilder->getProject()->id,
                    'projectTZBinet' => $taskDataArrayBuilder->getBinetProjectName(),
                    'taskText' => $taskData,
                ]);
                $task = $taskBuilder->build();
                $task->save();
            }
            return true;

        } catch (TaskDataException $exception) {
            $this->addError($exception->attribute, $exception->getMessage());
            return false;
        }
    }
}
