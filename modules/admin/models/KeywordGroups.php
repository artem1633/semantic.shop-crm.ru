<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "keyword_groups".
 *
 * @property int $id
 * @property string $name
 * @property int $semantik
 * @property int $frequency
 * @property string $add_keywords
 * @property int $project
 * @property int $status
 * @property int $import_key
 * @property string $in_work
 * @property int $connect_id
 * @property int $main_keywords
 * @property int $frequency_keywords
 * @property int $colorKey
 * @property int $colorFreq
 * @property int $count_keywords
 * @property int $max_frequency
 */
class KeywordGroups extends ActiveRecord
{
    /** @var string Выбранные ID*/
    public $ids;
    /**
     * {@inheritdoc}
     */
    const STATUS_ISXODNIE_GRUPPI = '1';
    const STATUS_V_RABOTE = '2';
    const STATUS_NA_MODERATSII = '3';
    const STATUS_OBRABOTKA_GRUPP = '4';
    const STATUS_PROVERKA_GRUPP = '5';
    const STATUS_GOTOVIE_GRUPPI = '6';
    const STATUS_ISPOLZOVANNIE = '7';

    public $conn_id;
    public $keys;
    public $keywords_count;

    public static function tableName()
    {
        return 'keyword_groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['semantik', 'frequency', 'project', 'status', 'import_key'], 'integer'],
            [['in_work','keysfrequency'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'name' => 'Название группы',
            'semantik' => 'Семантик',
            'frequency' => 'Частот',
            'add_keywords' => 'Дополнительные ключевики',
            'project' => 'Проект',
            'status' => 'Статус',
            'import_key' => 'Метка',
            'in_work' => 'Взято в работу',
            'keywords_count' => "Кол-во ключей",
            'conn_id' => 'Группы',
            'keys' => 'Ключевик',
            'comments' => 'Коментарии',
            'main_keywords' => 'Основные ключевики'
        ];
    }

    public function getStatuses()
    {
        return self::getStatusesLabel();
    }

    public static function getStatusesLabel()
    {
        return [

            self::STATUS_ISXODNIE_GRUPPI => 'Исходные группы',
            self::STATUS_V_RABOTE => 'В работе',
            self::STATUS_NA_MODERATSII=> 'На модерации',
            self::STATUS_OBRABOTKA_GRUPP => 'Обработка групп',
            self::STATUS_PROVERKA_GRUPP => 'Проверка групп',
            self::STATUS_GOTOVIE_GRUPPI => 'Готовые группы',
            self::STATUS_ISPOLZOVANNIE  => 'Использованные',
        ];
    }

    public function getImportKey()
    {
        return array(
            '1' => 'НЧ',
            '2' => 'ВЧ',
            '3' => 'На потом',
            '4' => 'Кроме на потом',

        );
    }

    public function getUserName($id)
    {
        return User::find()->where(['id' => $id])->one()['username'];
    }

    public static function getStatusName($id)
    {
        $array = self::getStatusesLabel();

        return $array[$id];
    }

    public function getTasksCountByStatus($status = false)
    {
        $count = (new Query())
            ->select('keyword_groups.id')
            ->from('keyword_groups')
            ->where(['project' => $this->id]);

        if ($status) {
            $count->andWhere(['status' => $status]);
        }

        return $count->count();
    }

    public function getCountKeywords()
    {
        $count = (new Query())
            ->select('keywords.id')
            ->from('keywords')
            ->where(['group_id' => $this->connect_id])
            ->andWhere(['status' => NULL]);

        return $count->count();
    }

    public function getCountKeywordsAdm()
    {
        $count = (new Query())
            ->select('keywords.id')
            ->from('keywords')
            ->where(['group_id' => $this->connect_id]);

        return $count->count();
    }

    public static function GetProjects(){
        $projects = Project::find()->orderBy('name')->all();
        return ArrayHelper::Map($projects, 'id', 'name');
    }

    public static function GetUsers(){
        $projects = User::find()->where(['role' => 'semantic'])->orderBy('username')->all();
        return ArrayHelper::Map($projects, 'id', 'username');
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert) {
        if (!$insert) {
            if ($this->isAttributeChanged('name',FALSE)) {
                $this->name_change = 1;
            }

            $stringKey = null;
            $colorKey = null;
            $stringFreq = null;
            $colorFreq  =null;
            $keys = Keywords::findAll(['group_id' => $this->connect_id]);
            $countKey=0;
            $hrYes = 0;
            /** @var Keywords $key */
            foreach ($keys as $key){
                if ($key->frequency >= 100) {
                    $hrYes=1;
                } else {
                    if ($hrYes==1) {
                        $colorKey = $colorKey . '<hr />';
                        $colorFreq = $colorFreq . '<hr />';
                        $hrYes = 0;
                    }
                }
                $countKey++;
                if($key->color){
                    $code = Colors::findOne(['id' => $key->color]);
                    $strKey = '<span style="color:'.$code->code.'">'.$key->keyword.'</span>';
                    $strFreq = '<span style="color:'.$code->code.'">'.$key->frequency.'</span>';
                }else{
                    $strKey = "<span>".$key->keyword."</span>";
                    $strFreq = '<span>'.$key->frequency.'</span>';
                }
                if($key->status){
                    $strKey = "<del>".$strKey."</del>".'<br />';
                    $strFreq = "<del>".$strFreq."</del>".'<br />';
                }else{
                    $strKey .= '<br />';
                    $strFreq .= '<br />';
                }

                $colorKey = $colorKey . $strKey;
                $colorFreq = $colorFreq . $strFreq;
                $stringKey = $stringKey . $key->keyword.' ';
                $stringFreq  = $stringFreq . $key->frequency.' ';
            }
            $this->main_keywords = $stringKey;
            $this->frequency_keywords = $stringFreq;
            $this->colorKey = $colorKey;
            $this->colorFreq = $colorFreq;
            $this->count_keywords = $countKey;
            $pos = strpos($stringFreq, "\n"); 
            if (!empty($pos)) {
            $line = trim(substr($stringFreq, 0, $pos));
            } else {
                $line = $stringFreq; 
            }
            $this->max_frequency = (int) $line;
        }
        //Убираем пустые строки в списке доп. ключевиков
        $string = $this->add_keywords ?? null;

        if ($string){
            $this->add_keywords = preg_replace( "#\s*?\r?\n\s*?(?=\r\n|\n)#s" , "" , $string );
            Yii::info('add_keywords before: '. $this->add_keywords, 'test');
        }
        return parent::beforeSave($insert);
    }
    
    public function getKeywords() {
        return $this->hasMany(Keywords::className(), ['group_id'=>'connect_id']);
        
    }

    public function getKeysfrequency()
    {
        $string = null;
                        $keys = Keywords::findAll(['group_id' => $this->connect_id]);

                        foreach ($keys as $key){
                            if($key->color){
                                $code = Colors::findOne(['id' => $key->color]);
                                $str = '<span style="color:'.$code->code.'">'.$key->frequency.'</span>';
                            }else{
                                $str = "<span>".$key->frequency."</span>";
                            }

                            if($key->status){
                                $str = "<del>".$str."</del>".'<br />';                            }else{
                                $str .= '<br />';
                            }

                            $string = $string . $str;
                        }
                        return $string;
    }

    public function getComments()
    {
        $string = null;
        $query = new Query;

        $query->select('*')
            ->from('comment')
            ->where(['entity' => (string)"group_id-{$this->id}"]);
        $rows = $query->all();
        if($rows){
            foreach ($rows as $q){
                $user = User::findOne(['id' => $q['created_by']]);
                $string = $string.'<div style="color: #333;margin-bottom: 10px">'
                    .$user->username.": ".$q['text'].'</div>';
            }
        }else{
            $string = '<span style="color: #aaaaaa"> Комментариев нет </span>';
        }

        return $string;
    }

    public static function getAlreadyExitsGroup($str,$id){

        //$groups = KeywordGroups::find()->where(['like','main_keywords' , $str.'='.'%',false])->andWhere(['project'=> $id])->all();
       // $key = Keywords::find()->where(['keyword' => $str])->andWhere(['project_id'=> $id])->all();
        $key = Keywords::find()->where(['keyword' => $str])->one();
        if(!$key){
            return false;
        }
        $group = KeywordGroups::find()->where(['connect_id' => $key->group_id])->one();

        if($group->project == $id){
            return true;
        }else{
            return false;
        }

    }
}
