<?php

namespace app\modules\admin\models\export;

use yii\base\Model;


class ExportProcessor extends Model
{
    /**
     *     При экспорте готового ТЗ если перед хотя бы перед одной группой есть число
     * (т.е. строка состоящая только из числа – без букв) и оно больше 300,
     * то при экспорте перед этим числом нужно добавлять разделитель в виде строки «ВЧ»
     * и перед ней и после нее должна быть пустая строка.
     *
     * Причем этот разделить должен добавляться только перед первым числом 300+ (если их несколько).
     */
    const LIMIT_FOR_DIVIDE = 300;

    protected function str_replace_first($from, $to, $content)
    {
        $from = '/' . preg_quote($from, '/') . '/';

        return preg_replace($from, $to, $content, 1);
    }

    public function process($text)
    {
        $output = $text;
         
//        //return substr(json_encode((string)$output), 1, -1);
//
//        //находим все цифры с новой строки
//        $re = '/^(\d+)/m';
//        preg_match_all($re, $output, $matches);
//
//        //находим первое больше 300
//        $matched = $matches[0] ?? [];
//
//        foreach ($matched as $integer) {
//
//            //добавляем перед ним ВЧ и возвращаем текст
//            if (intval($integer) > self::LIMIT_FOR_DIVIDE) {
//
//                $outputReplaced = str_replace("\r\n\r\n" . $integer, PHP_EOL ."\t". 'ВЧ' . PHP_EOL . PHP_EOL . $integer.PHP_EOL, $output);
//
//                if ($outputReplaced == $output) {
//                    //особый случай - первая строка (впереди нет PHP_EOL)
//                    $outputReplaced = $this->str_replace_first($integer, PHP_EOL ."\t".'ВЧ' . PHP_EOL . PHP_EOL . $integer.PHP_EOL, $output);
//                }
//
//                return $outputReplaced;
//            }
//
//        }

        return $output;
    }
}



