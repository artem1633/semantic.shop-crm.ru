<?php
/**
 * Created by PhpStorm.
 * User: Abbos
 * Date: 10/12/2018
 * Time: 4:07 PM
 */

namespace app\controllers;


use yii\web\Controller;

class GroupController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup', 'restore'],
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {


        return $this->redirect('site/status1?sort=fire');
    }

    public function actionProjectsStatus($status = false)
    {
        $projectsIds = (new Query())
            ->select('task.project_id AS id, project.name')
            ->distinct()
            ->from('task')
            ->join('LEFT JOIN', 'project', 'project_id = project.id')
            ->where(['task.status' => $status])
            ->andWhere(['task.user_id' => Yii::$app->user->identity->id])
            ->orderBy('project.name')
            ->all();

        foreach ($projectsIds as $key => &$projectsId){
            $projectsId = (int)$projectsId['id'];
        }

        if (!$projectsIds) {
            $projectsIds = ['-1'];
        }

        $searchModel = new ProjectSearch();
        $searchModel->projectsIds = $projectsIds;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('projectsstatus', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => Task::getStatusesLabel()[$status],
            'status' => $status,
        ]);
    }


    public function actionStatus1()
    {
        $searchModel = new TaskSearch();
        $searchModel->statusParam = '1';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->setSort([
            'defaultOrder' => [
                'deadline' => SORT_ASC
            ]
        ]);

        $dataProvider->sort->attributes['fire'] = [
            'asc' => ['fire' => SORT_DESC],
            'desc' => ['fire' => SORT_ASC],
        ];

        return $this->render('status1', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStatus2() {

        $searchModel = new TaskSearch();
        $searchModel->statusParam = '2';
        $searchModel->user_id = Yii::$app->user->identity->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->setSort([
            'defaultOrder' => [
                'deadline' => SORT_ASC
            ]
        ]);

        $dataProvider->sort->attributes['fire'] = [
            'asc' => ['fire' => SORT_DESC],
            'desc' => ['fire' => SORT_ASC],
        ];

        return $this->render('status2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionStatus4() {

        $searchModel = new TaskSearch();
        $searchModel->statusParam = '4';
        $searchModel->user_id = Yii::$app->user->identity->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->setSort([
            'defaultOrder' => [
                'deadline' => SORT_ASC
            ]
        ]);

        $dataProvider->sort->attributes['fire'] = [
            'asc' => ['fire' => SORT_DESC],
            'desc' => ['fire' => SORT_ASC],
        ];

        return $this->render('status4', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionUpdate($id) {

        $model = Task::findOne($id);

        $user = Yii::$app->user->id;

        $user = User::findOne(['id'=>$user]);

        if ($model->load(Yii::$app->request->post())) {
            $redirectUrl = self::generatePreviousUrl($model->id);

            if($model->isNewRecord && !$model->deadline){
                $model->deadline = date('Y-m-d', strtotime("+3 days"));
            }

            if ($model->deadline && !$model->isNewRecord) {
                if (!stripos($model->deadline, '-')) {
                    $model->deadline = date_create_from_format('d.m.Y', $model->deadline)->format('Y-m-d');
                }
            }

            $strings = explode(PHP_EOL, $model->main_keywords);

            $name = '';
            if (isset($strings[0]) && !is_numeric(trim($strings[0]))) {
                $name = trim($strings[0]);
            } elseif (isset($strings[1]) && !is_numeric(trim($strings[1]))) {
                $name = trim($strings[1]);
            }

            $model->name = $name;
            if ($model->getStatus() == '4') {
                if (
                    !$model->isNewRecord &&
                    !isset(Yii::$app->request->post()['all-ok']) &&
                    !isset(Yii::$app->request->post()['workButton']) &&
                    !isset(Yii::$app->request->post()['keylist']) &&
                    $user->role != 'admin'
                ) {
                    if (!$model->getMissedKeywords(true)) {
                        $modelId = $model->id;

                        $model->addError('task_text',
                            "Кажется в данном ТЗ была ошибка – не хватает более половины основных ключевиков. " .
                            "Рекомендуем <a href='view?id=$modelId' target='_blank'>перепроверить ТЗ.</a>");

                        Yii::$app->session->setFlash('error', "Кажется в данном ТЗ была ошибка – не хватает более половины основных ключевиков. " .
                            "Рекомендуем <a href='view?id=$modelId' target='_blank'>перепроверить ТЗ.</a>");

                        $model->status = '2';
                        $model->save();
                        return $this->refresh();
                    }
                }
            }

            $error = $model->name_unique_project();
            if ($error) {
                $model->addError('main_keywords', $error);

                return false;
            }

            if(isset(Yii::$app->request->post()['workButton']))
            {
                $model->save(false);

                return $this->render('update', [
                    'model' => $model,
                ]);
            }

            if ($model->getStatus() == '1') {
                $model->status = '4';
                $model->user_id = Yii::$app->user->identity->id;
            } else {
                $model->status = '2';
            }

            if ($model->save()) {
                return $this->redirect($redirectUrl);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', ['model' => Task::findOne($id),]);
    }

    public function actionSignup() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        $this->layout = '//main-signup';

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRestore() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = '//main-login';
        $model = new RestoreForm();

        $model->load(Yii::$app->request->post());

        if ($model->load(Yii::$app->request->post())) {
            if ($model->restore()) {
                return $this->render('password-restored');
            }
        }

        return $this->render('restore', [
            'model' => $model,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';

        $this->layout = '//main-login';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public static function generatePreviousUrl($taskId)
    {
        if (($model = Task::findOne($taskId)) !== null) {

            return ['status'.$model->status , 'TaskSearch[project_id]' => $model->project->id];
        }

        return ['index'];
    }

    public function actionInWork()
    {
        if (Yii::$app->request->post('selection')) {
            foreach(Yii::$app->request->post('selection') as $id){
                $task = Task::findOne(['id' => (int)$id]);
                $task->status = '4';
                $task->user_id = Yii::$app->user->id;
                $task->save(false);
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }
}