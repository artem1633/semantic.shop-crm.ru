<?php

namespace app\controllers;

use app\models\Comment;
use app\modules\admin\controllers\KeywordsSearch;
use app\modules\admin\models\Keywords;
use app\modules\admin\models\Project;
use app\modules\admin\models\ProjectSearch;
use Yii;
use app\modules\admin\models\KeywordGroups;
use app\modules\admin\controllers\KeywordGroupsSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KeywordGroupsController implements the CRUD actions for KeywordGroups model.
 */
class KeywordGroupsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup', 'restore'],
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];

    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Lists all KeywordGroups models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['semantik' => null]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KeywordGroups model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionShow()
    {
        $status = Yii::$app->request->get('status');
        $pid = Yii::$app->request->get('pid');
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['semantik' => Yii::$app->user->getId()]);
        $dataProvider->query->andWhere(['project' => $pid]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'status' => $status,
        ]);
    }

    /**
     * Creates a new KeywordGroups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KeywordGroups();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing KeywordGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $redirect = 'keyword-groups/control')
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([$redirect, 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing KeywordGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KeywordGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KeywordGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KeywordGroups::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionWork()
    {
        if (Yii::$app->request->post('keylist')) {
            foreach (Yii::$app->request->post('keylist') as $id) {
                $group = KeywordGroups::findOne(['id' => (int)$id]);
                $group->status = KeywordGroups::STATUS_V_RABOTE;
                $group->semantik = Yii::$app->user->id;
                $group->in_work = date('Y-m-d H:i:s');
                $group->save(false);
            }
        }

        return 'Ключевики взялись на работу.';
    }

    public function actionControl($id)
    {
        $group = KeywordGroups::findOne(['id' => $id]);
        $keywords = Keywords::findAll(['group_id' => $group->connect_id]);
        $searchModel = new KeywordsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['group_id' => $group->connect_id]);
        $dataProvider->query->andWhere(['status' => NULL]);
        $dataProvider->pagination = false;
        return $this->render('control', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'keywords' => $keywords,
            'model' => $group,

        ]);


    }

    public function actionCheck()
    {

        $post = Yii::$app->request->post('KeywordGroups');
        $model = KeywordGroups::findOne(['id' => $post['id']]);
        $model->add_keywords = $post['add_keywords'];
        $check_status = $model->status;
        if ($model->status == KeywordGroups::STATUS_OBRABOTKA_GRUPP) {
            $model->status++;
        }
        if ($model->status == KeywordGroups::STATUS_V_RABOTE) {
            $model->status++;
        }

        $model->save();
        Yii::$app->getSession()->setFlash('success', 'Группа отправилась на проверку!');
        if ($check_status == KeywordGroups::STATUS_OBRABOTKA_GRUPP) {
            return $this->redirect(['keyword-groups/group-processing-show', 'project_id' => $model->project]);
        }
        if ($check_status == KeywordGroups::STATUS_V_RABOTE) {
            return $this->redirect(['keyword-groups/in-work-show', 'project_id' => $model->project]);
        }
        if ($check_status == KeywordGroups::STATUS_NA_MODERATSII) {
            return $this->redirect(['keyword-groups/moderation-show', 'project_id' => $model->project]);
        }
        if ($check_status == KeywordGroups::STATUS_PROVERKA_GRUPP) {
            return $this->redirect(['keyword-groups/group-check-show', 'project_id' => $model->project]);
        }
        return $this->redirect(Yii::$app->request->referrer);


    }

    public function actionCheckAll()
    {
        if (Yii::$app->request->post('keylist')) {
            foreach (Yii::$app->request->post('keylist') as $id) {
                $group = KeywordGroups::findOne(['id' => (int)$id]);
                $group->status++;
                $group->save(false);
            }
            Yii::$app->getSession()->setFlash('success', 'Группы отправились на проверку!');
        }

        return true;
    }

    public function actionGroupProcess($id, $redirect = 'keyword-groups/group-process')
    {
        $group = KeywordGroups::findOne(['id' => $id]);
        $keywords = Keywords::findAll(['group_id' => $group->connect_id]);
        $searchModel = new KeywordsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['group_id' => $group->connect_id]);
        $dataProvider->query->andWhere(['status' => NULL]);
        $dataProvider->pagination = false;
        return $this->render('group_process', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'keywords' => $keywords,
            'model' => $group,
            'redirect' => $redirect,
        ]);
    }

    public function actionUsedGroup()
    {
        $status = KeywordGroups::STATUS_ISPOLZOVANNIE;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('used_group', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionGroup()
    {

        foreach ($_POST['keylist'] as $key) {
            $model = Keywords::findOne(['id' => $key]);
            $model->group_id = $_POST['id'][0];
            $model->color = 0;
            $model->save();

            $old_group = KeywordGroups::findOne(['id' => $_POST['model']]);
            $old_group->frequency -= $model->frequency;
            $old_group->save();

            $new_group = KeywordGroups::findOne(['connect_id' => $_POST['id'][0]]);
            $new_group->project = $old_group->project;
            $new_group->frequency += $model->frequency;
            $new_group->save();
        }
        Yii::$app->getSession()->setFlash('success', 'Ключевики перешли  в другую группу');
        return true;
    }

    public function actionOffer()
    {
        $use = 1;
        $keywds = Keywords::find()->where(['group_id' => $_POST['id']])->all();
        foreach ($keywds as $keys) {
            if ($keys->color != 0 && $keys->color > $use) {
                $use = $keys->color;
            }
        }
        $use++;
        if ($use == 10) {
            $use = 1;
        }
        foreach ($_POST['keylist'] as $key) {
            $model = Keywords::findOne(['id' => $key]);
            $group = KeywordGroupsSearch::findOne(['connect_id' => $model->group_id]);
            $model->color = $use;
            $model->save();
        }
        Yii::$app->getSession()->setFlash('success', 'Успешно');
        return true;
    }

    public function actionSetDefault()
    {
        $keywds = Keywords::find()->where(['group_id' => $_POST['id']])->all();
        foreach ($keywds as $keys) {
            $keys->color = 0;
            $keys->save();
        }
        Yii::$app->getSession()->setFlash('success', 'Успешно');
        return true;
    }

    public function actionNewGroup()
    {
        $newGroup = new KeywordGroups();
        $count = 0;
        if ($_POST) {
            $group_id = (new \yii\db\Query())
                ->select(['max(connect_id)'])
                ->from('keyword_groups')
                ->scalar();

            $color_count = (new \yii\db\Query())
                ->select(['color'])
                ->from('keyword_groups')
                ->orderBy(['id' => SORT_DESC])
                ->limit(1)
                ->scalar();

            $group_id += 11;
            $color_count++;
            if ($color_count <= 10) {
                $newGroup->color = $color_count;
            } else {
                $newGroup->color = 1;
            }

            $newGroup->name = $_POST['name'];
            $old_group = KeywordGroups::findOne(['id' => $_POST['model']]);
            $newGroup->status = $old_group->status;

            $newGroup->semantik = Yii::$app->user->getId();
            $newGroup->connect_id = $group_id;
            $newGroup->save();

            foreach ($_POST['keylist'] as $key) {
                $count++;
                $model = Keywords::findOne(['id' => $key]);
                $model->group_id = $group_id;


                $old_group = KeywordGroups::findOne(['id' => $_POST['model']]);
                $old_group->frequency -= $model->frequency;
                $old_group->save();

                if ($count == 1) {

                    if ((int)$model->frequency > 300) {
                        $newGroup->import_key = 2;
                    }

                    if ((int)$model->frequency <= 300) {
                        $newGroup->import_key = 1;
                    }
                }

                $newGroup->project = $old_group->project;
                $newGroup->frequency += $model->frequency;
                $model->save();
                $newGroup->save();
            }
            Yii::$app->getSession()->setFlash('success', 'Создано новая группа');
            return true;
        }

        Yii::$app->getSession()->setFlash('error', 'Ошибка при создание новой группы');
        return false;
    }

    public function actionDeleteKey()
    {
        foreach ($_POST['keylist'] as $key) {

            $model = Keywords::findOne(['id' => $key]);
            $gid = $model->group_id;
            $model->status = Keywords::STATUS_DELETED;
            $model->save(false);
            $group = KeywordGroups::findOne(['connect_id' => $gid]);
            $group->frequency -= $model->frequency;
            $group->save(false);

        }
        Yii::$app->getSession()->setFlash('success', 'Ключевики удалены!');
        return true;
    }

    public function actionInWork()
    {
        $status = KeywordGroups::STATUS_V_RABOTE;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('in_work', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionInWorkShow()
    {
        $id = $_GET['id'] ?? null;

        if ($id) {
            $model = Comment::getNewComment($id);
        } else {
            $model = new Comment();
        }

        $status = KeywordGroups::STATUS_V_RABOTE;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $dataProvider->query->andWhere(['semantik' => Yii::$app->user->getId()]);
        $pageSize = Yii::$app->session->get('pageSizeWork', 50);
        $dataProvider->pagination->pageSize = $pageSize;

        return $this->render('in_work_show', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionModeration()
    {
        $status = KeywordGroups::STATUS_NA_MODERATSII;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('moderation', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionModerationShow()
    {
        $id = $_GET['id'] ?? null;

        if ($id) {
            $model = Comment::getNewComment($id);
        } else {
            $model = new Comment();
        }

        $status = KeywordGroups::STATUS_NA_MODERATSII;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $dataProvider->query->andWhere(['semantik' => Yii::$app->user->getId()]);
        $pageSize = Yii::$app->session->get('pageSizeSource', 50);
        $dataProvider->pagination->pageSize = $pageSize;

        return $this->render('moderation_show', [
            'model'=>$model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGroupProcessingShow()
    {
        $id = $_GET['id'] ?? null;

        if ($id) {
            $model = Comment::getNewComment($id);
        } else {
            $model = new Comment();
        }

        $status = KeywordGroups::STATUS_OBRABOTKA_GRUPP;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $dataProvider->query->andWhere(['semantik' => Yii::$app->user->getId()]);
        $pageSize = Yii::$app->session->get('pageSizeProcessing', 50);
        $dataProvider->pagination->pageSize = $pageSize;

        if (!$dataProvider->totalCount > 0) {
            return $this->redirect(['keyword-groups/group-processing']);
        }

        return $this->render('group_processing_show', [
            'model'=>$model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionGroupProcessing()
    {

        $status = KeywordGroups::STATUS_OBRABOTKA_GRUPP;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('group_processing', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionGroupCheckShow()
    {
        $id = $_GET['id'] ?? null;

        if ($id) {
            $model = Comment::getNewComment($id);
        } else {
            $model = new Comment();
        }

        $status = KeywordGroups::STATUS_PROVERKA_GRUPP;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $dataProvider->query->andWhere(['semantik' => Yii::$app->user->getId()]);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('group_check_show', [
            'model'=>$model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionGroupCheck()
    {

        $status = KeywordGroups::STATUS_PROVERKA_GRUPP;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('group_check', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionFinalGroupShow()
    {
        $id = $_GET['id'] ?? null;

        if ($id) {
            $model = Comment::getNewComment($id);
        } else {
            $model = new Comment();
        }

        $status = KeywordGroups::STATUS_GOTOVIE_GRUPPI;
        $project = Project::findOne(['id' => $_GET['project_id']]);
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $dataProvider->query->andWhere(['semantik' => Yii::$app->user->getId()]);
        $pageSize = Yii::$app->session->get('pageSizeFinal', 50);
        $dataProvider->pagination->pageSize = $pageSize;

        return $this->render('final_group_show', [
            'model'=>$model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'project' => $project,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionFinalGroup()
    {
        $status = KeywordGroups::STATUS_GOTOVIE_GRUPPI;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('final_group', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionSourceGroups()
    {

        $status = KeywordGroups::STATUS_ISXODNIE_GRUPPI;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('source_groups', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionSourceGroupsShow()
    {
        $model = new Comment();
        if ($model->load(yii::$app->request->post())) {

            $model->entity = 'group_id-' . $_GET['id'];
            $model->created_by = Yii::$app->user->id;
            $model->updated_by = Yii::$app->user->id;
            $model->created_at = time();
            $model->updated_at = time();
            $model->save();
        }

        $pageSize = Yii::$app->session->get('pageSizeSource') ?? null;
        if (!$pageSize) {
            $pageSize = 50;
        }

        $status = KeywordGroups::STATUS_ISXODNIE_GRUPPI;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        if(!Yii::$app->request->get('KeywordGroupsSearch')['import_key']){
            $dataProvider->query->andWhere(['<>' ,'import_key' , '3']);
        }


        $dataProvider->pagination->pageSize = $pageSize;

        return $this->render('source_groups_show', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSetCountPage()
    {
        Yii::$app->session->set($_POST['pageSizeName'], $_POST['pageSize']);
        return 'Выбранные группы приняты!';
    }
}
