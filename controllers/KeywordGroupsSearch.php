<?php

namespace app\controllers;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\KeywordGroups;

/**
 * KeywordGroupsSearch represents the model behind the search form of `app\modules\admin\models\KeywordGroups`.
 */
class KeywordGroupsSearch extends KeywordGroups
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'semantik', 'frequency', 'project', 'status', 'import_key'], 'integer'],
            [['name', 'add_keywords', 'in_work'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KeywordGroups::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'semantik' => $this->semantik,
            'project' => $this->project,
            'status' => $this->status,
            'import_key' => $this->import_key,
            'in_work' => $this->in_work,
            
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['>=','count_keywords', $this->count_keywords])
                ->andFilterWhere(['>=','frequency', $this->frequency])
                
            ->andFilterWhere(['like', 'add_keywords', $this->add_keywords]);
        

        return $dataProvider;
    }
}
