<?php

use yii\db\Migration;

/**
 * Handles the creation of table `options`.
 */
class m180715_100547_create_options_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('options', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique(),
            'value' => $this->string()->notNull(),
        ]);

        $this->insert('options', [
            'key' => 'site_email',
            'value' => 'test@test.com',
        ]);

        $this->insert('options', [
            'key' => 'site_email_name',
            'value' => 'Test',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('options');
    }
}
