<?php

use yii\db\Migration;

/**
 * Class m181014_074205_add_new_column_to_keyword_groups
 */
class m181014_074205_add_new_column_to_keyword_groups extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('keyword_groups', 'color', $this->integer());
    }

    public function down()
    {
        echo "m181014_074205_add_new_column_to_keyword_groups cannot be reverted.\n";

        return false;
    }
}
