<?php

use yii\db\Migration;

/**
 * Class m181020_153950_create_table_tz_binet_connect
 */
class m181020_153950_create_table_tz_binet_connect extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('tz_binet_connect', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer(),
            'name' => $this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable('tz_binet_connect');
    }

}
