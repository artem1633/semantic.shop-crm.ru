<?php

use yii\db\Migration;

/**
 * Class m181218_074201_add_new_column_to_keyword_groups
 */
class m181218_074201_add_new_column_to_keyword_groups extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('keyword_groups', 'name_change',
            $this->integer(1)->defaultValue(0)->comment('Название изменялось'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181218_074201_add_new_column_to_keyword_groups cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181218_074201_add_new_column_to_keyword_groups cannot be reverted.\n";

        return false;
    }
    */
}
