<?php

use yii\db\Migration;

/**
 * Class m181011_155629_create_table_keyword_groups
 */
class m181011_155629_create_table_keyword_groups extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('keyword_groups', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'semantik' => $this->integer(),
            'frequency' => $this->integer(),
            'add_keywords' => $this->string(),
            'project' => $this->integer(),
            'status' => $this->integer(),
            'import_key' => $this->integer(),
            'in_work' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        $this->dropTable('keyword_groups');
    }
}
