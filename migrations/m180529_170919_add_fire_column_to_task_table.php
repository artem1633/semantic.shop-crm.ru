<?php

use yii\db\Migration;

/**
 * Handles adding fire to table `task`.
 */
class m180529_170919_add_fire_column_to_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'fire', $this->integer(1)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('task', 'fire');
    }
}
