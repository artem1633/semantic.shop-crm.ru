<?php

use yii\db\Migration;

/**
 * Class m190613_180405_alter_table_keywords_drop_column
 */
class m190613_180405_alter_table_keywords_drop_column extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->dropColumn('keywords', 'project_id');
    }

    public function down()
    {
        echo "m190613_180405_alter_table_keywords_drop_column cannot be reverted.\n";

        return false;
    }

}
