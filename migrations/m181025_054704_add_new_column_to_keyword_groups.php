<?php

use yii\db\Migration;

/**
 * Class m181025_054704_add_new_column_to_keyword_groups
 */
class m181025_054704_add_new_column_to_keyword_groups extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('keyword_groups', 'main_keywords', $this->text());
    }

    public function down()
    {
        echo "m181025_054704_add_new_column_to_keyword_groups cannot be reverted.\n";

        return false;
    }

}
