<?php

use yii\db\Migration;

/**
 * Class m190122_140110_add_new_column_keywordgroup
 */
class m190122_140110_add_new_column_keywordgroup extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
$this->addColumn('keyword_groups', 'max_frequency',
            $this->integer(11)->comment('Первий частотник'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190122_140110_add_new_column_keywordgroup cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190122_140110_add_new_column_keywordgroup cannot be reverted.\n";

        return false;
    }
    */
}
