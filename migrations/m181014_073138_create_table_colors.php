<?php

use yii\db\Migration;

/**
 * Class m181014_073138_create_table_colors
 */
class m181014_073138_create_table_colors extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {


        $this->createTable('colors', [
            'id' => $this->primaryKey(),
            'code' => $this->string(),
        ]);

        $this->insert('colors', [
            'code' => '#A0D468',
        ]);
        $this->insert('colors', [
            'code' => '#4FC1E9',
        ]);
        $this->insert('colors', [
            'code' => '#5D9CEC',
        ]);
        $this->insert('colors', [
            'code' => '#AC92EC',
        ]);
        $this->insert('colors', [
            'code' => '#EC87C0',
        ]);

        $this->insert('colors', [
            'code' => '#ED5565',
        ]);
        $this->insert('colors', [
            'code' => '#FC6E51',
        ]);
        $this->insert('colors', [
            'code' => '#FFCE54',
        ]);
        $this->insert('colors', [
            'code' => '#8CC152',
        ]);
        $this->insert('colors', [
            'code' => '#3BAFDA',
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('colors');
    }
}
