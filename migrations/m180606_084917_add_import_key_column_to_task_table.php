<?php

use yii\db\Migration;

/**
 * Handles adding import_key to table `task`.
 */
class m180606_084917_add_import_key_column_to_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'import_key', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('task', 'import_key');
    }
}
