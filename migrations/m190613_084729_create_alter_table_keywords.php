<?php

use yii\db\Migration;

/**
 * Class m190613_084729_create_alter_table_keywords
 */
class m190613_084729_create_alter_table_keywords extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('keywords', 'project_id', $this->integer());
    }

    public function down()
    {
        echo "m190613_084729_create_alter_table_keywords cannot be reverted.\n";

        return false;
    }

}
