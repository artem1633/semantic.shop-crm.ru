<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180427_183900_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user', [
            'id' => $this->primaryKey(),
			'email' => $this->string()->notNull()->unique(),
			'username' => $this->string(),			
			'phone' => $this->string(),
			'password' => $this->string()->notNull(),
			'registration_date' => $this->date()->notNull(),
            'role' => $this->string(),
        ], $tableOptions);
		
		$this->insert('user', [
			'email' => 'admin@admin',
			'username' => 'admin',
            'password' => md5('admin'), // '21232f297a57a5a743894a0e4a801fc3',
			'role' => 'admin',
			'registration_date' => '2018-01-01',			
        ]);
		
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
