<?php

use yii\db\Migration;

/**
 * Class m190107_065014_add_new_to_keyword_groups
 */
class m181229_065014_add_new_to_keyword_groups extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('keyword_groups', 'frequency_keywords',
            $this->text()->null()->comment('Колиство частот'));
        $this->addColumn('keyword_groups', 'colorKey',
            $this->text()->null()->comment('Ключевики отформатированние'));
        $this->addColumn('keyword_groups', 'colorFreq',
            $this->text()->null()->comment('Колиство частот отформатированние'));
        $this->addColumn('keyword_groups', 'count_keywords',
            $this->integer()->comment('Количество ключевиков'));
        $this->alterColumn('keyword_groups', 'add_keywords', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190107_065014_add_new_to_keyword_groups cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190107_065014_add_new_to_keyword_groups cannot be reverted.\n";

        return false;
    }
    */
}
