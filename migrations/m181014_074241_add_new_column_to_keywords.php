<?php

use yii\db\Migration;

/**
 * Class m181014_074241_add_new_column_to_keywords
 */
class m181014_074241_add_new_column_to_keywords extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('keywords', 'color', $this->integer());
    }

    public function down()
    {
        echo "m181014_074241_add_new_column_to_keywords cannot be reverted.\n";

        return false;
    }
}
