<?php

use app\modules\admin\models\KeywordGroups;
use app\modules\admin\models\Project;
use app\modules\admin\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'В работе';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' =>
        ['/keyword-groups/in-work']
];
$this->params['breadcrumbs'][] = Project::getProjectNameById($_GET['project_id']);
?>
<style>
    hr {
        margin-top: 3px;
        margin-bottom: 3px;
    }
</style>
<div class="keyword-groups-index">
    <h3><?= "Проект: " . $this->params['breadcrumbs'][1]; ?></h3>
    <p></p>
    <div class="panel panel-default panel-body">

        <span class="btn btn-success" onClick="offer()">Сдать на проверку</span>

        <?php Pjax::begin(['id' => 'pjax_grid']); ?>
        <span class="pull-right"> на странице </span>
        <span class="pull-right"> <?= Html::dropDownList('count_per_page', Yii::$app->session->get('pageSizeWork', 50),
                [20 => '20', 30 => '30', 50 => '50', 100 => '100', 200 => '200', 500 => '500'],
                ['id' => 'per-page', 'class' => 'form-control-sm', 'onChange' => 'set_count_page()']) ?> </span>
        <span class="pull-right"> Показывать по </span>

        <?php
        try {
            echo GridView::widget([
                'id' => 'grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'name' => 'checked',
                        'checkboxOptions' => function ($model) {
                            return ['value' => $model->id];
                        },
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'headerOptions' => ['style' => 'text-align:center;']
                    ],
                    [
                        'attribute' => 'comments',
                        'value' => function ($model) {
                            return $model->comments . '<div><a href="#" class="add-comment" onclick="addComment(' . $model->id . ')">Комментировать</a></div>';
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 15%'],
                    ],
                    [
                        'attribute' => 'name',
                        'content' => function ($data) {
                            return Html::a($data->name, ['keyword-groups/control', 'id' => $data->id]);

                        },
                        'contentOptions' => ['style' => 'width:330px'],
                        'filter' => false,
                    ],
                    [
                        'attribute' => 'main_keywords',
                        'format' => 'raw',
                        'content' => function ($model) {
                            return $model->colorKey;
                        },
                        'contentOptions' => ['style' => 'width: 40%;'],
                        'filter' => true,
                    ],
                    [
                        'attribute' => 'frequency_keywords',
                        'label' => "Частот ключа",
                        'content' => function ($model) {
                            return $model->colorFreq;
                        },
                        'format' => 'raw',
                        'filter' => true,
                    ],
                    [
                        'attribute' => 'count_keywords',
                        'label' => "Кол-во" . '<br />' . "ключей",
                        'encodeLabel' => false,
                        'contentOptions' => ['style' => 'width: 5%;'],
                        'filter' => true,
                    ],


                    [
                        'attribute' => 'semantik',
                        'value' => function (KeywordGroups $model) {
                            /** @var User $user */
                            $user = User::findOne(['id' => $model->semantik]);
                            return $user->username;
                        },
                        'filter' => false,
                    ],
                    [
                        'attribute' => 'in_work',
                        'filter' => false,
                    ],

                ],

            ]);
        } catch (Exception $e) {
            echo $e->getMessage();
        } ?>
        <div class="counter">Выбрано:&nbsp;<p class="checked">0</p></div>

        <?php Pjax::end(); ?>
    </div>
</div>
<script type="text/javascript">

    function offer() {
        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }
        var dialog = confirm('Сдалть на проверку ?');
        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'check-all',
                data: {keylist: keys},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

    function set_count_page() {


        $.ajax({
            type: "POST",
            url: 'set-count-page',
            data: {pageSizeName: 'pageSizeWork', pageSize: $('#per-page').val()},
            success: function (result) {
                $.pjax.reload({container: '#pjax_grid'});
            }
        });
    }
</script>
<?php require('_add_comment.php') ?>
