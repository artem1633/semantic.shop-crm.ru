<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$title =  \app\modules\admin\models\KeywordGroups::getStatusesLabel();

$this->title =$title[(int)$status];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keyword-groups-index">
    <?= Yii::$app->session->getFlash('error'); ?>

    <p>
    </p>
    <div class="panel panel-default panel-body">

    <?php Pjax::begin(['id' => 'pjax_grid']); ?>
        <?php if(!isset($_GET['status'])):?>
             <span class="btn btn-success" onClick="group()">Взять в работу</span>
        <?php endif;?>
        <p>
        </p>

    <?= GridView::widget([
            'id' => 'grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\CheckboxColumn',
                'name' => 'checked',
                'checkboxOptions' => function($model) {
                    return ['value' => $model->id];
                },
                'contentOptions' => ['style' => 'text-align:center;'],
                'headerOptions' => ['style' => 'text-align:center;']
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function ($data) {
                        if($data->status == \app\modules\admin\models\KeywordGroups::STATUS_V_RABOTE)
                            return Html::a($data->name, ['keyword-groups/control', 'id' => $data->id]);



                    return Html::a($data->name, ['keyword-groups/group-process', 'id' => $data->id]);
                },
                'contentOptions' => ['style' => 'text-align:center white-space: nowrap;;'],

            ],
            [
                'attribute' => 'semantik',
                'content' => function($model){
                  return isset($model->semantik) ? "<span style='color:green'>".$model->getUserName($model->semantik)."</span>" : '<span style="color:red">Свободен</span>';

                },
                'contentOptions' => ['style' => 'text-align:center white-space: nowrap;;'],
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'prompt' => 'Все',
                ],
                'filter'=> \app\modules\admin\models\KeywordGroups::GetUsers(),

            ],
            'frequency',
            [
                'attribute' => 'project',
                'value' => function($searchModel){
                    $project = \app\modules\admin\models\Project::findOne(['id' => $searchModel->project]);
                    return $project->name;
                },
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'prompt' => 'Все',
                ],
                'filter'=> \app\modules\admin\models\KeywordGroups::GetProjects(),

            ],
            [
                'attribute' => 'status',
                'value' => function($searchModel){
                    $a = \app\modules\admin\models\KeywordGroups::getStatusesLabel();
                    return $a[$searchModel->status];
                },
                'filter' => false,

            ],
            [
                'attribute' => 'in_work',
                'value' => function($model){

                    return isset($model->in_work) ? date("d.m.Y", strtotime($model->in_work)) : '';
                },
                'filter' => false,

            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
</div>


<script type="text/javascript">


    function group() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Взять в работу?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'work',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }


</script>


