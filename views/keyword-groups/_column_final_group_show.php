<?php

use app\modules\admin\models\User;

return
    [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'yii\grid\CheckboxColumn',
            'name' => 'checked',
            'checkboxOptions' => function ($model) {
                return ['value' => $model->id];
            },
            'contentOptions' => ['style' => 'text-align:center;'],
            'headerOptions' => ['style' => 'text-align:center;']
        ],
        [
            'attribute' => 'comments',
            'value' => function ($model) {
                return $model->comments . '<div><a href="#" class="add-comment" onclick="addComment(' . $model->id . ')">Комментировать</a></div>';
            },
            'format' => 'raw',
            'contentOptions' => ['style' => 'width: 15%'],
        ],
        [
            'attribute' => 'name',
            'content' => function ($data) {
                return $data->name;

            },
            'contentOptions' => ['style' => 'width:5%;white-space: nowrap;'],
            'filter' => false,
        ],
        [
            'attribute' => 'main_keywords',
            'format' => 'raw',
            'content' => function ($model) {
                return $model->colorKey;
            },
            'contentOptions' => ['style' => 'width: 40%;'],
            'filter' => true,
        ],
        [
            'attribute' => 'frequency_keywords',
            'label' => "Частот ключа",
            'content' => function ($model) {
                return $model->colorFreq;
            },
            'format' => 'raw',
            'filter' => true,
        ],
        [
            'attribute' => 'count_keywords',
            'label' => "Кол-во" . '<br />' . "ключей",
            'encodeLabel' => false,
            'contentOptions' => ['style' => 'width: 5%;'],
            'filter' => true,
        ],
        [
            'attribute' => 'semantik',
            'value' => function ($model) {
                $user = User::findOne(['id' => $model->semantik]);
                return $user->username;
            },
            'filter' => false,
        ],
        [
            'attribute' => 'in_work',
            'filter' => false,
        ],
    ];
?>


