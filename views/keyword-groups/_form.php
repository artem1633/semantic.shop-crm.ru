<?php

use app\modules\admin\models\Project;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\KeywordGroups */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="keyword-groups-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default panel-body">

        <div class="row">

            <div class="col-md-4">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-4">
    <?= $form->field($model, 'frequency')->textInput() ?>
            </div>
            <div class="col-md-12">
    <?= $form->field($model, 'add_keywords')->textArea(['maxlength' => true, 'rows' => 10])  ?>
            </div>
            <div class="col-md-4">
    <?php $projects = Project::find()->orderBy('name')->all(); ?>

    <?=
    $form->field($model, 'project')->widget(Select2::classname(), [
        'data' => ArrayHelper::Map($projects, 'id', 'name'),
        'options' => ['placeholder' => 'Выберите проект...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
            </div>

            <div class="col-md-4">
    <?= $form->field($model, 'status')->dropDownList($model->statuses); ?>
            </div>

            <div class="col-md-4">
    <?= $form->field($model, 'import_key')->dropDownList($model->importKey, ['prompt' => 'Выберите значение...']); ?>
            </div>
            </div>

    </div>
    <div class="form-group">
        <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
