<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => $title, 'url' =>
    ['/keyword-groups/group-processing']];
$this->params['breadcrumbs'][] = \app\modules\admin\models\Project::getProjectNameById($_GET['project_id']);
?>
<style>
    hr {
        margin-top: 3px;
        margin-bottom: 3px;
    }
</style>
<div class="keyword-groups-index">
    <h3><?="Проект: " . \app\modules\admin\models\Project::getProjectNameById($_GET['project_id'])
        ?></h3>
    <p>

    </p>

    <div class="panel panel-default panel-body">


        <?php Pjax::begin(['id' => 'pjax_grid']); ?>
        <span class="pull-right"> на странице </span>
        <span class="pull-right"> <?=Html::dropDownList('count_per_page',Yii::$app->session->get('pageSizeProcessing',50),
                [20=>'20',30=>'30',50=>'50',100=>'100',200=>'200',500=>'500'],
                ['id'=>'per-page','class'=>'form-control-sm','onChange'=>'set_count_page()'])?> </span>
        <span class="pull-right"> Показывать по </span>

        <?= GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'name' => 'checked',
                    'checkboxOptions' => function($model) {
                        return ['value' => $model->id];
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'headerOptions' => ['style' => 'text-align:center;']
                ],
                [
                    'attribute' => 'comments',
                    'value' => function ($model) {
                        return   $model->comments . '<div><a href="#" class="add-comment" onclick="addComment('.$model->id.')">Комментировать</a></div>';
                    },
                    'format'=>'raw',
                    'contentOptions' => ['style' => 'width: 15%'],
                ],
                [
                    'attribute' => 'name',
                    'content' => function ($data) {
                        return Html::a($data->name, ['keyword-groups/group-process', 'id' => $data->id]);
                    },
                    'contentOptions' => ['style' => 'width: 5%'],
                    'filter' => false,
                ],
                [
                    'attribute' => 'main_keywords',
                    'format'=>'raw',
                    'content' => function($model){
                        return $model->colorKey;
                    },
                    'contentOptions' => ['style' => 'width: 40%;'],
                    'filter' => true,
                ],
                [
                    'attribute' => 'frequency_keywords',
                    'label' => "Частот ключа",
                    'content' => function($model){
                        return $model->colorFreq;
                    },
                    'format' => 'raw',
                    'filter' => true,
                ],
                [
                    'attribute' => 'count_keywords',
                    'label' => "Кол-во".'<br />'."ключей",
                    'encodeLabel'=>false,
                    'contentOptions' => ['style' => 'width: 5%;'],
                    'filter' => true,
                ],
                [
                    'attribute' => 'semantik',
                    'value' => function ($model){
                        $user = \app\modules\admin\models\User::findOne(['id' => $model->semantik]);
                        return $user->username;
                    },
                    'filter' => false,
                ],
                [
                    'attribute' => 'in_work',
                    'filter' => false,
                ],
            ],

        ]); ?>
        <div class="counter">Выбрано:&nbsp;<p class="checked">0</p></div>

        <?php Pjax::end(); ?>
    </div>
</div>


<script type="text/javascript">


    function to_moderation() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Отправить на доработку?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'to-moderation',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function to_accept() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Принять выбранные группы?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'to-accept',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function set_count_page() {


        $.ajax({
            type: "POST",
            url: 'set-count-page',
            data: {pageSizeName: 'pageSizeProcessing',pageSize: $('#per-page').val()},
            success: function (result) {
                $.pjax.reload({container: '#pjax_grid'});
            }
        });
    }

</script>

<?php require ('_add_comment.php')?>