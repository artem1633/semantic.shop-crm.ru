<?php

use app\modules\admin\models\Project;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Исходные группы';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' =>
        ['/keyword-groups/source-groups']
];
$this->params['breadcrumbs'][] = Project::getProjectNameById($_GET['project_id']);
Yii::info($this->params, 'test');
?>
    <style>
        hr {
            margin-top: 3px;
            margin-bottom: 3px;
        }
    </style>
    <div class="keyword-groups-index">

        <h3><?= "Проект: " . $this->params['breadcrumbs'][0]['label'] ?></h3>
        <div class="panel panel-default panel-body">


            <?php Pjax::begin(['id' => 'pjax_grid']); ?>
            <span class="pull-right"> на странице </span>
            <span class="pull-right"> <?= Html::dropDownList('count_per_page',
                    Yii::$app->session->get('pageSizeSource', 50),
                    [20 => '20', 30 => '30', 50 => '50', 100 => '100', 200 => '200', 500 => '500'],
                    ['id' => 'per-page', 'class' => 'form-control-sm', 'onChange' => 'set_count_page()']) ?> </span>
            <span class="pull-right"> Показывать по </span>
            <?php if (!isset($_GET['status'])): ?>
                <span class="btn btn-success" onClick="group()">Взять в работу</span>
            <?php endif; ?>
            <?php
            try {
                echo GridView::widget([
                    'id' => 'grid',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'class' => 'yii\grid\CheckboxColumn',
                            'name' => 'checked',
                            'checkboxOptions' => function ($model) {
                                return ['value' => $model->id];
                            },
                            'contentOptions' => ['style' => 'text-align:center;'],
                            'headerOptions' => ['style' => 'text-align:center;']
                        ],
                        [
                            'attribute' => 'comments',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->comments . '<div><a href="#" class="add-comment" onclick="addComment(' . $model->id . ')">Комментировать</a></div>';
                            },
                            'contentOptions' => ['style' => 'width: 14%'],
                        ],
                        [
                            'attribute' => 'name',
                            'content' => function ($data) {
                                return Html::a($data->name, ['keyword-groups/control', 'id' => $data->id]);
                            },
                            'contentOptions' => ['style' => 'width: 23%;white-space: nowrap;'],
                        ],
                        [
                            'attribute' => 'main_keywords',
                            'format' => 'raw',
                            'content' => function ($model) {
                                return $model->colorKey;
                            },
                            'contentOptions' => ['style' => 'width: 38%;'],
                            'filter' => true,
                        ],
                        [
                            'attribute' => 'max_frequency',
                            'label' => "Частот ключа",
                            'content' => function ($model) {
                                return $model->colorFreq;
                            },
                            'format' => 'raw',
                            'filter' => true,
                        ],
                        [
                            'attribute' => 'count_keywords',
                            'label' => "Кол-во" . '<br />' . "ключей",
                            'encodeLabel' => false,
                            'contentOptions' => ['style' => 'width: 4%;'],
                            'filter' => true,
                        ],
                        [
                            'attribute' => 'frequency',
                            'filter' => true,
                        ],
                        [
                            'attribute' => 'import_key',
                            'value' => function ($searchModel) {
                                $a = $searchModel->getImportKey();
                                return $a[$searchModel->import_key];
                            },
                            'filterInputOptions' => [
                                'class' => 'form-control',
                                'prompt' => 'Кроме на потом',
                            ],
                            'contentOptions' => ['style' => 'width: 6%;'],
                            'filter' => array(
                                '5' => 'Все',
                                '1' => 'НЧ',
                                '2' => 'ВЧ',
                                '3' => 'На потом',

                            ),
                        ],

                    ],

                ]);
            } catch (Exception $e) {
                echo $e->getMessage();
            } ?>
            <div class="counter">Выбрано:&nbsp;<p class="checked">0</p></div>

            <?php Pjax::end(); ?>
        </div>
    </div>
<?php
$this->registerCss("
.form-control{
padding: 6px 3px !important;
}
");
?>
    <script type="text/javascript">


        function group() {

            var keys = $('#grid').yiiGridView('getSelectedRows');

            if (keys == '') {
                swal({
                    title: "",
                    text: 'Отметьте флажками необходимые Группы',
                    confirmButtonColor: "#337ab7"
                });
                return;
            }

            var dialog = confirm('Взять в работу?');

            if (dialog == true) {

                $.ajax({
                    type: "POST",
                    url: 'work',
                    data: {keylist: keys},
                    success: function (result) {
                        swal({
                            title: "",
                            text: result,
                            confirmButtonColor: "#337ab7"
                        });
                        $.pjax.reload({container: '#pjax_grid'});
                    }
                });
            }
        }

        function set_count_page() {


            $.ajax({
                type: "POST",
                url: 'set-count-page',
                data: {pageSizeName: 'pageSizeSource', pageSize: $('#per-page').val()},
                success: function (result) {
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    </script>
<?php require('_add_comment.php') ?>