<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use rmrevin\yii\module\Comments;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->name;

$url = null;
$project_url = null;
if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_ISXODNIE_GRUPPI){
    $project_url = '/keyword-groups/source-groups-show';
    $url = '/keyword-groups/source-groups';
}

if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_V_RABOTE){
    $url = '/keyword-groups/in-work';
    $project_url = '/keyword-groups/in-work-show';
}

if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_NA_MODERATSII) {
    $url         = '/keyword-groups/moderation';
    $project_url = '/keyword-groups/moderation-show';
}
if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_OBRABOTKA_GRUPP) {
    $url         = '/keyword-groups/group-processing';
    $project_url = '/keyword-groups/group-processing-show';
}

if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_PROVERKA_GRUPP){
    $url         = '/keyword-groups/group-check';
    $project_url = '/keyword-groups/group-check-show';
}

if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_GOTOVIE_GRUPPI){
    $url = '/keyword-groups/final-group';
    $project_url = '/keyword-groups/final-group-show';
}

if($model->status == \app\modules\admin\models\KeywordGroups::STATUS_ISPOLZOVANNIE){
    $url = '/keyword-groups/used-group';
    $project_url = '/keyword-groups/used-group-show';
}

$this->params['breadcrumbs'][] = ['label' =>
    \app\modules\admin\models\KeywordGroups::getStatusName($model->status),'url' =>$url ];
$this->params['breadcrumbs'][] =  ['label' => \app\modules\admin\models\Project::getProjectNameById
($model->project), 'url' => [$project_url, 'project_id' => $model->project]] ;
$this->params['breadcrumbs'][] = $model->name;


?>
<div class="keyword-groups-index">

    <h3><?="Проект: " . \app\modules\admin\models\Project::getProjectNameById($model->project)
        ?></h3>
    <p>
    </p>
    <div class="panel panel-default panel-body">
        <h1><?=$model->name;?> </h1>
        <?= Html::a('Редактировать название', ['update', 'id' => $model->id,'redirect'=>$redirect], ['class' => 'btn btn-primary']) ?>
        <span class="btn btn-success" onClick="offer()">Предложить выделить</span>
        <span class="btn btn-warning" onClick="setDefault()">Сбросить предложенные</span>
        <span class="btn btn-danger" onClick="deleteKey()"><i class="fa fa-trash"></i> Удалить</span>

        <input type="hidden" id="color-ids" value="<?=$model->connect_id?>">
        <?php
        \yii\bootstrap\Modal::begin([
            'header' => 'Группы',
            'size'=>'modal-lg',
            'toggleButton' =>
                [
                        'label' => '<i class="fa fa-upload"></i> Переместить в другую групппу',
                        'tag' => 'button',
                        'class' => 'btn btn-info',
                ],
        ]);
        ?>
        <div id="changeGroup">
            <?php $form = ActiveForm::begin(['action' => 'group']); ?>


            <?php $projects = \app\modules\admin\models\KeywordGroups::find()->where(['project' =>$model->project])->orderBy('name')->all(); ?>
            <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id ])->label(false); ?>
            <?=
            $form->field($model, 'conn_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::Map($projects, 'connect_id', 'name'),
                'options' => ['placeholder' => 'Название'],
                'pluginOptions' => [
                    'maximumSelectionLength'=> 1,
                    'allowClear' => false,
                    'multiple' => true,
                    'size' => 12,
                ],
            ]);
            ?>



            <div class="form-group">
                <span class="btn btn-success" onClick="group()">Переместить</span>
                <span class="btn btn-info" onClick="toggleGroup()">Создать новую группу</span>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <div id="newGroup" style="display: none;">
            <?php $form = ActiveForm::begin(['action' => 'group']); ?>


            <?php $projects = \app\modules\admin\models\KeywordGroups::find()->orderBy('name')->all(); ?>
            <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id ])->label(false); ?>
            <?= $form->field($model, 'conn_id')->hiddenInput(['value'=> $model->connect_id ])->label(false);?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true,'value' => '']) ?>



            <div class="form-group">
                <span class="btn btn-success" onClick="newGroup()">Создать</span>
                <span class="btn btn-info" onClick="toggleGroup2()">Выбрать из списка</span>
            </div>

            <?php ActiveForm::end(); ?>
        </div>


        <?php \yii\bootstrap\Modal::end(); ?>
        <p>
        </p>
        <h3>Основные ключевики</h3>
        <?= GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'rowOptions'=>function($model){
                if($model->color){
                    $code = \app\modules\admin\models\Colors::findOne(['id' => $model->color]);
                    return ['style' => 'background:'.$code->code];
                }
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'name' => 'checked',
                    'checkboxOptions' => function($model) {
                        return ['value' => $model->id];
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'headerOptions' => ['style' => 'text-align:center;']
                ],
                [
                    'attribute' => 'keyword',
                    'filterInputOptions' => [
                        'class'       => 'form-control',
                        'placeholder' => 'Искать ключевик...'
                    ]
                ],
                [
                    'attribute' => 'frequency',
                    'filter' => false,
                ],
            ],
        ]); ?>



    </div>
    <?php $form = ActiveForm::begin(['action' => 'check']); ?>
    <div class="panel panel-default panel-body">


        <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id ])->label(false); ?>
        <?= $form->field($model, 'add_keywords')->textArea(['maxlength' => true, 'rows' => 10])  ?>

    </div>
    <div class="form-group">
        <?= Html::submitButton('Сдать на проверку', ['class' => 'btn btn-success']) ?>

    </div>

    <?php ActiveForm::end(); ?>
</div>

<?= Comments\widgets\CommentListWidget::widget([
    'entity' => (string)"group_id-{$model->id}", // type and id
]); ?>

<?php
$this->registerJs('
$(document).ready(function(){
    $(\'.modal-dialog\').click(function(){
    
        $(\'.s2-select-label\').hide();
    });
});
');
?>

<script type="text/javascript">

    function offer() {
        $('form').submit(function (e) {
            e.preventDefault();
        });
        var keys = $('#grid').yiiGridView('getSelectedRows');
        var id = $('#color-ids').val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

            $.ajax({
                type: "POST",
                url: 'offer',
                data: {keylist: keys,id:id},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });

    }
    function group() {
        $('form').submit(function (e) {
            e.preventDefault();
        });
        var keys = $('#grid').yiiGridView('getSelectedRows');
        var  id = $('#keywordgroups-conn_id').val();
        var model_id = $('#keywordgroups-id').val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Переместить в новую группу?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'group',
                data: {keylist: keys,id:id,model:model_id},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

    function newGroup() {
        $('form').submit(function (e) {
            e.preventDefault();
        });
        var keys = $('#grid').yiiGridView('getSelectedRows');
        var  id = $('#keywordgroups-conn_id').val();
        var model_id = $('#keywordgroups-id').val();
        var name = $('#keywordgroups-name').val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Создать новую группу?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'new-group',
                data: {keylist: keys,id:id,model:model_id,name:name},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

    function toggleGroup() {
        var x = document.getElementById("changeGroup");
        var b = document.getElementById("newGroup");
        x.style.display = "none";
        b.style.display = "block";

    }

    function toggleGroup2() {
        var x = document.getElementById("changeGroup");
        var b = document.getElementById("newGroup");
        x.style.display = "block";
        b.style.display = "none";
    }

    function deleteKey() {

        var keys = $('#grid').yiiGridView('getSelectedRows');


        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ключевики',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('точно Удалить?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'delete-key',
                data: {keylist: keys},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

    function setDefault(){
        $('form').submit(function (e) {
            e.preventDefault();
        });
        var id = $('#color-ids').val();

        var dialog = confirm('Сбросить предложенные?');
        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'set-default',
                data: {id:id},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

</script>


