
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => $title, 'url' =>
    ['/keyword-groups/final-group']];
$this->params['breadcrumbs'][] = \app\modules\admin\models\Project::getProjectNameById($_GET['project_id']);
?>
<style>
    hr {
        margin-top: 3px;
        margin-bottom: 3px;
    }
</style>
<div class="keyword-groups-index">


    <h3><?="Проект: " . \app\modules\admin\models\Project::getProjectNameById($_GET['project_id'])
        ?></h3>

    <p>
    </p>
    <div class="panel panel-default panel-body">



        <?php Pjax::begin(['id' => 'pjax_grid']); ?>
        <span class="pull-right"> на странице </span>
        <span class="pull-right"> <?=Html::dropDownList('count_per_page',Yii::$app->session->get('pageSizeFinal',50),
                [20=>'20',30=>'30',50=>'50',100=>'100',200=>'200',500=>'500'],
                ['id'=>'per-page','class'=>'form-control-sm','onChange'=>'set_count_page()'])?> </span>
        <span class="pull-right"> Показывать по </span>

        <?php
        try {
            echo GridView::widget([
                'id' => 'grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => require(__DIR__ . '/_column_final_group_show.php'),
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'error');
            echo $e->getMessage();
        } ?>
        <div class="counter">Выбрано:&nbsp;<p class="checked">0</p></div>

        <?php Pjax::end(); ?>
    </div>
</div>
<script>

function set_count_page() {
    $.ajax({
    type: "POST",
    url: 'set-count-page',
    data: {pageSizeName: 'pageSizeFinal',pageSize: $('#per-page').val()},
        success: function (result) {
            $.pjax.reload({container: '#pjax_grid'});
        }
    });
}

</script>

<?php require ('_add_comment.php')?>