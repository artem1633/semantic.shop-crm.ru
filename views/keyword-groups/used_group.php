<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => $title, 'url' =>
    ['/keyword-groups/used-group']];
?>
<div class="project-index">

    <h1><?= Html::encode($title) ?></h1>

    <p></p>
    <div class="panel panel-default panel-body">


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'filter' => false,

                ],
                [
                    'attribute' => 'count_used',
                    'value' => function ($model) {

                        return $model->getCountUsedGroups() ." / ". $model->allowed_groups;

                    },
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'width: 10%; white-space: nowrap;'],
                ],
            ],
        ]); ?>


    </div>
</div>