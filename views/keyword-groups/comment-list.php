<?php
/**
 * comment-list.php
 * @author Revin Roman
 * @link https://rmrevin.ru
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $CommentsDataProvider
 */

use rmrevin\yii\fontawesome\FA;
use rmrevin\yii\module\Comments;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Markdown;
use yii\helpers\Url;

/** @var Comments\widgets\CommentListWidget $CommentListWidget */
$CommentListWidget = $this->context;

$comments = [];

/** @var Comments\models\Comment $CommentModel */
$CommentModel = \Yii::createObject(Comments\Module::instance()->model('comment'));

if ($CommentListWidget->showCreateForm && $CommentModel::canCreate()) {
    echo Html::tag('h3', Yii::t('app', 'Add comment'), ['class' => 'comment-title']);

    echo Comments\widgets\CommentFormWidget::widget([
        'theme' => $CommentListWidget->theme,
        'entity' => $CommentListWidget->entity,
        'Comment' => $CommentModel,
        'anchor' => $CommentListWidget->anchorAfterUpdate,
    ]);
}

$CommentListWidget
    ->getView()
    ->registerJs('jQuery("#' . $CommentListWidget->options['id'] . '").yiiCommentsList(' . Json::encode($comments) . ');');
