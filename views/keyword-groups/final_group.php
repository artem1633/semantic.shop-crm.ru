<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = $title;
$this->params['breadcrumbs'][] = $title;
?>
<div class="project-index">

    <h1><?= Html::encode($title) ?></h1>

    <p></p>
    <div class="panel panel-default panel-body">


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'summary' => false,
            'rowOptions'=>function ($model){
                if(!$model->getGroupByStatus(\app\modules\admin\models\KeywordGroups::STATUS_GOTOVIE_GRUPPI)){
                    return array('hidden' => 'hidden');
                }

            },
            'columns' => [

                [
                    'attribute' => 'name',
                    'value' => function ($model) {

                        return Html::a(
                            Html::encode($model->name) . ' (' .
                            Yii::$app->formatter->asRaw('<span style="color: #333333">'.$model->getGroupByStatus(\app\modules\admin\models\KeywordGroups::STATUS_GOTOVIE_GRUPPI) . '</span>' )
                            . ')',
                            \yii\helpers\Url::to(["final-group-show", 'project_id' => $model->id]));

                    },
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'width: 96%;color:red; white-space: nowrap;'],
                ],
            ],
        ]); ?>


    </div>
</div>
