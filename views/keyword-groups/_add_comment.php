
<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;

/* @var $model app\models\Comment */

Modal::begin([
    'id' => 'addComment',
    'header' => 'Коментарии',
    'size'=>'modal-lg',

]);
?>
    <div class="comments-widget"><h3 class="comment-title">Добавить комментарий</h3>
        <a name="commentcreateform"></a>
        <div class="row comment-form">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'text')->textarea(['maxlength' => true,'value' => '']) ?>

                <button type="submit" class="btn btn-primary">Добавить комментарий</button>
            </div>

            <?php ActiveForm::end(); ?>


        </div>
    </div>

<?php
Modal::end();
?>

<script type="text/javascript">
    function addComment(id) {
        var action = $('#w0').attr('action');
        action = action + "&id="+id;
        $('#w0').attr('action',action);

        $('#addComment').modal('show');
    }
</script>
