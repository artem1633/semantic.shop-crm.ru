<?php
use app\modules\admin\models\KeywordGroups;
use app\modules\admin\models\Task;

if ($this->context->route=='keyword-groups/group-process' || $this->context->route=='keyword-groups/control') {
    $keywordsGroup = KeywordGroups::find()->where(['id'=>Yii::$app->request->get()])->one();        
    $status = $keywordsGroup->status;
}
if ($this->context->route=='site/update' ) {
    $task = Task::find()->where(['id'=>Yii::$app->request->get()])->one();        
    $statusTask = $task->status;
}
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Главное меню', 'options' => ['class' => 'header']],
                    ['label' => 'ТЗ',
                        'items' => [
                            [
                                'label' => 'Исходные ключевики',
                                'icon' => 'calendar-o',
                                'url' => ['site/status1', 'sort' => 'fire'],
                                'active' => Yii::$app->request->get('status') == 1 ||
                                    $this->context->route == 'site/status1' || (isset($statusTask) && $statusTask == Task::STATUS_SOURCE_KEYS),
                            ],
                            [
                                'label' => 'В работе',
                                'icon' => 'calendar-plus-o',
                                'url' => ['site/projects-status', 'status' => '4'],
                                'active' => Yii::$app->request->get('status') == 4 ||
                                    $this->context->route == 'site/status4' || (isset($statusTask) && $statusTask == Task::STATUS_IN_WORK),

                            ],
                            [
                                'label' => 'Готовое ТЗ',
                                'icon' => 'calendar-check-o',
                                'url' => ['site/projects-status', 'status' => '2'],
                                'active' => Yii::$app->request->get('status') == 2 ||
                                    $this->context->route == 'site/status2' || (isset($statusTask) && $statusTask == Task::STATUS_DONE),
                            ],
                        ],
                    ],

                    [
                        'label' => 'Семантика',
                        'icon' => 'align-justify',

                        'items' => [
                            [
                                'label' => 'Исходные группы',
                                'url' => ['keyword-groups/source-groups'],
                                'active' => $this->context->route == 'keyword-groups/source-groups' ||
                                    $this->context->route == 'keyword-groups/source-groups-show'|| (isset($status) && $status == KeywordGroups::STATUS_ISXODNIE_GRUPPI),
                            ],
                            [
                                'label' => 'В работе',

                                'url' => ['keyword-groups/in-work'],
                                'active' => $this->context->route == 'keyword-groups/in-work' ||
                                    $this->context->route == 'keyword-groups/in-work-show' || (isset($status) && $status ==KeywordGroups::STATUS_V_RABOTE),
                            ],
                            [
                                'label' => 'На модерации',

                                'url' => ['keyword-groups/moderation'],
                                'active' => $this->context->route == 'keyword-groups/moderation' ||
                                    $this->context->route == 'keyword-groups/moderation-show'|| (isset($status) && $status ==KeywordGroups::STATUS_NA_MODERATSII),
                            ],
                            [
                                'label' => 'Обработка групп',

                                'url' => ['keyword-groups/group-processing'],

                                'active' => $this->context->route == 'keyword-groups/group-processing' ||
                                    $this->context->route == 'keyword-groups/group-processing-show'|| (isset($status) && $status ==KeywordGroups::STATUS_OBRABOTKA_GRUPP),
                            ],
                            [
                                'label' => 'Проверка групп',

                                'url' => ['keyword-groups/group-check'],

                                'active' => $this->context->route == 'keyword-groups/group-check' ||
                                    $this->context->route == 'keyword-groups/group-check-show' || (isset($status) && $status ==KeywordGroups::STATUS_PROVERKA_GRUPP),
                            ],
                            [
                                'label' => 'Готовые группы',

                                'url' => ['keyword-groups/final-group'],

                                'active' => $this->context->route == 'keyword-groups/final-group' ||
                                    $this->context->route == 'keyword-groups/final-group-show'  || (isset($status) && $status ==KeywordGroups::STATUS_GOTOVIE_GRUPPI),
                            ],
                            [
                                'label' => 'Использованные',

                                'url' => ['keyword-groups/used-group'],

                                'active' => $this->context->route == 'keyword-groups/used-group' ||
                                    $this->context->route == 'keyword-groups/used-group-show' || (isset($status) && $status ==KeywordGroups::STATUS_ISPOLZOVANNIE),
                            ],

                        ],
                    ],

                ],
            ]
        ) ?>

    </section>

</aside>
