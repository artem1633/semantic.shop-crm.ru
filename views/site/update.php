<?php

use app\modules\admin\controllers\TaskController;
use app\modules\admin\models\Task;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\module\Comments;


$this->title = 'Изменение: ' . $model->name;

$this->params['breadcrumbs'][] = ['label' => Task::getStatusesLabel()[$model->status], 'url' =>  TaskController::generatePreviousUrl($model->id)];
$this->params['breadcrumbs'][] = \app\modules\admin\models\Project::getProjectNameById($model->project_id);
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
?>

<div class="task-form">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default panel-body">

        <div class="row">

            <div class="col-md-4">
                <?= Html::activeLabel($model, 'project_id') . ': ' . $model->projectName ?> <br>

                <?= Html::activeLabel($model, 'project_TZBinet') . ': ' . $model->project_TZBinet ?>
            </div>

            <div class="col-md-4">
                <?= Html::activeLabel($model, 'date_add') . ': ' . $model->dateAddFormat ?> <br>

                <?= Html::activeLabel($model, 'deadline') . ': ' . $model->deadlineFormat ?>
            </div>

            <div class="col-md-4">
                <?= Html::activeLabel($model, 'status') . ': ' . $model->statusName ?>
            </div>

        </div>

        <br>
        <?= Html::textArea('', $model->keywords,
            ['id' => 'keywords', 'rows' => 15, 'style' => 'width:100%', 'readonly' => 'readonly']) ?>

        <div class="form-group">
            <span class="btn btn-default" onClick="setSelection()">Выделить все</span>
        </div>

        <?= $form->field($model, 'task_text',
            ['errorOptions' => ['class' => 'help-block', 'encode' => false]])->textArea([
            'maxlength' => true,
            'rows' => 20,
            'errorOptions' => ['class' => 'help-block', 'encode' => false]
        ]) ?>

        <?php if ($model->task_text) {
            ?>
            <label class="control-label" for="task-missed">Потерянные ключевики</label>
            <?= Html::textArea('task-missed', $model->getMissedKeywords(), [
                'label' => 'Потерянные ключевики',
                'template' => "{label}\n{input}\n{hint}\n{error}",
                'id' => 'keywords',
                'rows' => 15,
                'style' => 'width:100%',
                'disabled' => true
            ]) ?>

        <?php } ?>
    </div>

    <div class="form-group">
        <?php if ($model->getStatus() == '1'): ?>
            <?= Html::submitButton('Взять в работу', [
                'class' => 'btn btn-info'
            ]) ?>
        <?php else: ?>
            <?= Html::submitButton('Сдать на проверку', [
                'class' => 'btn btn-success'
            ]) ?>
        <?php endif; ?>
        <?php
        \yii\bootstrap\Modal::begin([
            'header' => '<h3>Не хватает более половины основных ключевиков. Перепроверить ТЗ?</h3>',
            'id' => 'myModal'
        ]);
        ?>
        <div class="form-group">
            <?= Html::submitButton('Все в порядке',
                ['class' => 'btn btn-gray','name'=>'all-ok' ,'id' => 'close-button']) ?>
            <?= Html::a('Перепроверить ТЗ', ['view', 'id'=>$model->id], ['target' => '_blank',
                'class' => 'btn btn-primary ', 'id'=>'forward-look-a', 'style' => 'float:right']) ?>
        </div>

        <?php \yii\bootstrap\Modal::end(); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?= Comments\widgets\CommentListWidget::widget([
    'entity' => (string)"task-{$model->id}", // type and id
]); ?>


<?PHP

Yii::$app->view->registerJs('var taskId = "' . $model->id . '"', \yii\web\View::POS_HEAD);

$modalLoadScript = <<< JS
if ($('.alert-danger').is(":visible")) {
        $('#myModal').modal().show();
    }
if ($('#task-task_text').attr('aria-invalid')) {
        $('#myModal').modal().show();
    }
    
$('#forward-look').on('click', function( element ) {
  element.preventDefault();
  
  $('#close-button').click();
})    
JS;

$this->registerJs($modalLoadScript, \yii\web\View::POS_READY);
?>

<script type="text/javascript">

    function setSelection() {

        var target = document.getElementById('keywords');
        var rng, sel;
        if (document.createRange) {
            rng = document.createRange();
            rng.selectNode(target)
            sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(rng);
            //document.execCommand("Copy");
        } else {
            var rng = document.body.createTextRange();
            rng.moveToElementText(target);
            rng.select();
        }
    }

</script>















