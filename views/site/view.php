<?php

use app\modules\admin\controllers\TaskController;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\module\Comments;
use app\modules\admin\models\Task;

$user = Yii::$app->user->identity;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Task::getStatusesLabel()[$model->status], 'url' =>  TaskController::generatePreviousUrl($model->id)];
$this->params['breadcrumbs'][] = \app\modules\admin\models\Project::getProjectNameById($model->project_id);
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];

if($model->status == Task::STATUS_DONE){
$script = <<< JS
    $('[href^="/site/projects-status?status=2"]').parent().addClass("active");
    $('[href^="/site/projects-status?status=2"]').parent().parent().parent().addClass("menu-open");
    $('[href^="/site/projects-status?status=2"]').parent().parent().show();
JS;
$this->registerJs($script, yii\web\View::POS_READY);
}
?>

<div class="task-form">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if ($model->status == '2' || $model->status =='1') {
        ?>
        <p>
            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        </p>
    <?php } ?>
    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default panel-body">

        <div class="row">

            <div class="col-md-4">
                <?= Html::activeLabel($model, 'project_id') . ': ' . $model->projectName ?> <br>

                <?= Html::activeLabel($model, 'project_TZBinet') . ': ' . $model->project_TZBinet ?>
            </div>

            <div class="col-mFd-4">
                <?= Html::activeLabel($model, 'date_add') . ': ' . $model->dateAddFormat ?> <br>

                <?= Html::activeLabel($model, 'deadline') . ': ' . $model->deadlineFormat ?>
            </div>

            <div class="col-md-4">
                <?= Html::activeLabel($model, 'status') . ': ' . $model->statusName ?>
            </div>

        </div>

        <br>
        <?= Html::textArea('', $model->keywords,
            ['id' => 'keywords', 'rows' => 15, 'style' => 'width:100%', 'disabled' => true]) ?>

        <div class="form-group">
        </div>
        <?= $form->field($model, 'task_text')->textArea(['maxlength' => true, 'rows' => 20, 'disabled' => true]) ?>
        <label class="control-label" for="task-missed">Потерянные ключевики</label>
        <?= Html::textArea('task-missed', $model->getMissedKeywords(), [
            'label' => 'Потерянные ключевики',
            'template' => "{label}\n{input}\n{hint}\n{error}",
            'id' => 'keywords',
            'rows' => 15,
            'style' => 'width:100%',
            'disabled' => true
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?= Comments\widgets\CommentListWidget::widget([
    'entity' => (string)"task-{$model->id}", // type and id
]); ?>

<script type="text/javascript">

    function setSelection() {

        var target = document.getElementById('keywords');
        var rng, sel;
        if (document.createRange) {
            rng = document.createRange();
            rng.selectNode(target)
            sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(rng);
            //document.execCommand("Copy");
        } else {
            var rng = document.body.createTextRange();
            rng.moveToElementText(target);
            rng.select();
        }
    }

</script>

















