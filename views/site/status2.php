<?php

use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'Готовое ТЗ';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = \app\modules\admin\models\Project::getProjectNameById($project_id);
?>
<div class="task-index">

    <div class="panel panel-default panel-body">

        <?php $projects = (new \yii\db\Query())
            ->select(['project_id AS id', 'project.name AS name'])
            ->distinct()
            ->from('task')
            ->join('LEFT JOIN', 'project', 'project_id = project.id')
            ->where(['status' => '2'])
            ->orderBy('project.name')
            ->all(); ?>

        <?php Pjax::begin(['id' => 'pjax_grid']); ?>

        <?=
        GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'fire',
                    'label' => Html::img('@web/images/fire.png', ['height' => '19px', 'width' => 'auto']),
                    'encodeLabel' => false,
                    'content' => function ($data) {
                        if ($data->fire == 1) {
                            return Html::img('@web/images/fire.png', ['height' => '20px', 'width' => 'auto']);
                        }
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'headerOptions' => ['style' => 'text-align:center;'],
                ],
                [
                    'attribute' => 'name',
                    'value' => function ($model) {
                        $modelColor = $model->getMissedKeywords(true);
                        $add = [];

                        if (!$modelColor) {
                            $add = ['style' => 'color:#8A2BE2'];
                        }

                        return Html::a(
                            Html::encode($model->name) .
                            \app\modules\admin\widgets\CommentCountWidget::widget(['task' => $model]),
                            Url::to(["view?id={$model->id}"]),
                            $add
                        );
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'semantic',
                    'value' => function ($model) {
                        return $model->getUserNameByID($model->user_id);
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'status',
                    'content' => function ($data) {
                        return $data->statusName;
                    },
                ],
                [
                    'attribute' => 'project_id',
                    'content' => function ($data) {
                        return $data->projectName;
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'project_id',
                        'data' => ArrayHelper::Map($projects, 'id', 'name'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'options' => [
                            'placeholder' => ''
                        ]
                    ]),
                ],
                'project_TZBinet',
                [
                    'attribute' => 'date_add',
                    'content' => function ($data) {
                        return date('d.m.Y H:i', strtotime($data->date_add));
                    }
                ],
                [
                    'attribute' => 'deadline',
                    'content' => function ($data) {
                        return date('d.m.Y', strtotime($data->deadline));
                    },
                    'contentOptions' => function ($data) {
                        if ($data->deadline < date('Y-m-d')) {
                            return ['style' => 'color:red;'];
                        } else {
                            return [];
                        }
                    },
                ],
                [
                    'content' => function ($data) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ["view?id={$data->id}"],
                                ['title' => 'Просмотр',]) .
                            Html::a('<span class="glyphicon glyphicon-pencil"></span>', ["update?id={$data->id}"],
                                ['title' => 'Редактирование',]);
                    }
                ],
            ],
        ]);
        ?>

        <?php Pjax::end(); ?>

    </div>

</div>








