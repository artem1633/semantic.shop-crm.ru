<?php

use app\modules\admin\models\Project;
use kartik\select2\Select2;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\module\Comments;
/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\KeywordGroups */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="keyword-groups-form">
    <h3><?="Проект: " . \app\modules\admin\models\Project::getProjectNameById($model->project)
        ?></h3>
    <div class="form-group">
        <div class="panel panel-default panel-body">
        <?= Html::a('Добавить Основной ключевик ', ['keywords/create','id'=>$model->id], ['class' => 'btn btn-primary']) ?>
        <?php
        $query = \app\modules\admin\models\Keywords::find()->where(['group_id' => $model->connect_id]);
        $provider = new ActiveDataProvider([
            'query' => $query,

        ]);
        ?>
        <h1>Основные ключевики</h1>

        <span class="btn btn-danger" onClick="deleteKey()"><i class="fa fa-trash"></i> Удалить</span>
        <span class="btn btn-warning" onClick="setDefault()">Сбросить предложенные</span>
        <input type="hidden" id="color-ids" value="<?=$model->connect_id?>">

        <?php
        \yii\bootstrap\Modal::begin([
            'header' => 'Группы',
            'size'=>'modal-lg',
            'toggleButton' => ['label' => '<i class="fa fa-upload"></i> Переместить в другую групппу',
                'tag' => 'button',
                'class' => 'btn btn-info',],
        ]);
        ?>
        <div id="changeGroup">
            <?php $form = ActiveForm::begin(['action' => 'group']); ?>


            <?php $projects = \app\modules\admin\models\KeywordGroups::find()->where(['project' =>$model->project])->orderBy('name')->all(); ?>
            <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id ])->label(false); ?>
            <?=
            $form->field($model, 'conn_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::Map($projects, 'connect_id', 'name'),
                'options' => ['placeholder' => 'Название'],
                'pluginOptions' => [
                    'allowClear' => false,
                    'multiple' => true,

                ],
            ]);
            ?>



            <div class="form-group">
                <span class="btn btn-success" onClick="group()">Переместить</span>
                                            <span class="btn btn-info" onClick="toggleGroup()">Создать новую группу</span>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <div id="newGroup" style="display: none;">
            <?php $form = ActiveForm::begin(['action' => 'group']); ?>


            <?php $projects = \app\modules\admin\models\KeywordGroups::find()->orderBy('name')->all(); ?>
            <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id ])->label(false); ?>
            <?= $form->field($model, 'conn_id')->hiddenInput(['value'=> $model->connect_id ])->label(false);?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true,'value' => '']) ?>



            <div class="form-group">
                <span class="btn btn-success" onClick="newGroup()">Создать</span>
                <span class="btn btn-info" onClick="toggleGroup2()">Выбрать из списка</span>
            </div>

            <?php ActiveForm::end(); ?>
        </div>


        <?php \yii\bootstrap\Modal::end(); ?>


        <p>
        </p>




        <?= GridView::widget([
            'id' => 'grid',
            'dataProvider' => $provider,
            'rowOptions'=>function($provider){
                if($provider->color){
                    $code = \app\modules\admin\models\Colors::findOne(['id' => $provider->color]);
                    return ['style' => 'background:'.$code->code];
                }
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'name' => 'checked',
                    'checkboxOptions' => function($provider) {
                        return ['value' => $provider->id];
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'headerOptions' => ['style' => 'text-align:center;']
                ],
                [
                    'attribute' => 'keyword',
                    'content' => function ($provider){
                        $string = $provider->keyword;

                            if($provider->status){
                                $string = "<del>".$provider->keyword."</del>".'<br />';
                            }



                        return $string;

                    },
                    'filterInputOptions' => [
                        'class'       => 'form-control',
                        'placeholder' => 'Искать ключевик...'
                    ]
                ],
                [
                    'attribute' => 'frequency',
                    'content' => function ($provider){
                        $string = $provider->frequency;

                        if($provider->status){
                            $string = "<del>".$provider->frequency."</del>".'<br />';
                        }



                        return $string;

                    },
                    'filter' => false,
                ],
            ],
        ]); ?>
        </div>
    </div>

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default panel-body">

        <div class="row">

            <div class="col-md-4">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-4">
    <?= $form->field($model, 'frequency')->textInput() ?>
            </div>
            <div class="col-md-12">

            </div>
            <div class="col-md-12">
    <?= $form->field($model, 'add_keywords')->textArea(['maxlength' => true, 'rows' => 10])  ?>
            </div>
            <div class="col-md-4">
    <?php $projects = Project::find()->orderBy('name')->all(); ?>

    <?=
    $form->field($model, 'project')->widget(Select2::classname(), [
        'data' => ArrayHelper::Map($projects, 'id', 'name'),
        'options' => ['placeholder' => 'Выберите проект...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
            </div>

            <div class="col-md-4">
    <?= $form->field($model, 'status')->dropDownList($model->statuses); ?>
            </div>

            <div class="col-md-4">
    <?= $form->field($model, 'import_key')->dropDownList($model->importKey, ['prompt' => 'Выберите значение...']); ?>
            </div>
            </div>

    </div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <input type="hidden" id="model_id" name="model_id" value=<?=$model->id?>>
    <span class="btn btn-info" onClick="to_moderation()"><i class="fa fa-close"></i> На доработку</span>

    <?php ActiveForm::end(); ?>

</div>
<?= Comments\widgets\CommentListWidget::widget([
    'entity' => (string)"group_id-{$model->id}", // type and id
]); ?>

<?php
$this->registerJs('
$(document).ready(function(){
    $(\'.modal-dialog\').click(function(){
    
        $(\'.s2-select-label\').hide();
    });
   
});
');
?>


<script type="text/javascript">


    function to_moderation() {

        var keys = $('#model_id').val();

            $.ajax({
                type: "POST",
                url: 'to-moderation',
                data: {model_id: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });

    }
    function group() {
        $('form').submit(function (e) {
            e.preventDefault();
        });
        var keys = $('#grid').yiiGridView('getSelectedRows');
        var  id = $('#keywordgroups-conn_id').val();
        var model_id = $('#keywordgroups-id').val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Переместить в новую группу?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'group',
                data: {keylist: keys,id:id,model:model_id},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

    function newGroup() {
        $('form').submit(function (e) {
            e.preventDefault();
        });
        var keys = $('#grid').yiiGridView('getSelectedRows');

        var model_id = $('#keywordgroups-id').val();
        var name = $('#keywordgroups-name').val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Создать новую группу?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'new-group',
                data: {keylist: keys,model:model_id,name:name},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

    function toggleGroup() {
        var x = document.getElementById("changeGroup");
        var b = document.getElementById("newGroup");
        x.style.display = "none";
        b.style.display = "block";

    }

    function toggleGroup2() {
        var x = document.getElementById("changeGroup");
        var b = document.getElementById("newGroup");
        x.style.display = "block";
        b.style.display = "none";
    }

    function deleteKey() {

        var keys = $('#grid').yiiGridView('getSelectedRows');


        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые ключевики',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('точно Удалить?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'delete-key',
                data: {keylist: keys},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

    function setDefault(){
        $('form').submit(function (e) {
            e.preventDefault();
        });
        var id = $('#color-ids').val();

        var dialog = confirm('Сбросить предложенные?');
        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'set-default',
                data: {id:id},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }



</script>

