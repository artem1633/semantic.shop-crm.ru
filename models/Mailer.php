<?php

namespace app\models;

use app\modules\admin\models\Options;
use yii\swiftmailer\Mailer as BaseMailer;

/**
 * Class Mailer
 * @package common\yii\swiftmailer
 */
class Mailer extends BaseMailer
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $hostname = Options::findOne(['key' => 'email_host'])->value;
        $username = Options::findOne(['key' => 'site_email'])->value;
        $password = Options::findOne(['key' => 'site_email_password'])->value;
        $port = Options::findOne(['key' => 'email_port'])->value;
        $encryption = Options::findOne(['key' => 'email_encryption'])->value;

        if ($hostname && $username && $password) {
            $this->setTransport([
                'class' => 'Swift_SmtpTransport',
                'host' => $hostname,
                'username' => $username,
                'password' => $password,
                'port' => $port,
                'encryption' => $encryption,
            ]);
        }
    }
}
