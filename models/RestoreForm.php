<?php

namespace app\models;

use app\modules\admin\models\Options;
use Yii;
use yii\base\Model;
use app\modules\admin\models\User;

/**
 * Signup form
 */
class RestoreForm extends Model
{

    public $email;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'trim'],
            ['email', 'email'],
            [
                'email',
                'exist',
                'targetClass' => 'app\modules\admin\models\User',
                'message' => 'Такой Email не существует в базе.'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
        ];
    }

    /**
     * Signs user up.
     *
     * @return true
     */
    public function restore()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = User::findOne(['email' => $this->email]);

        $randomPassword = $this->randomPassword();

        if ($user) {
            $user->password = $randomPassword;
            $user->save();

            $this->sendEmail($user, $randomPassword);
        }

        return true;
    }

    private function randomPassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = [];
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    /**
     * @param User $user
     * @param $password
     */
    public function sendEmail($user, $password)
    {
        Yii::$app->mailer->compose()
            ->setFrom([Options::findOne(['key' => 'site_email'])->value => Options::findOne(['key' => 'site_email_name'])->value])
            ->setTo($user->email)
            ->setSubject('Восстановление пароля')
            ->setTextBody('Восстановление пароля')
            ->setHtmlBody("<p>Уважаемый $user->username, пароль успешно восстановлен ваш новый пароль - $password. </p>")
            ->send();
    }
}
