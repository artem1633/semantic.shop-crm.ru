<?php

namespace tests\modules\admin\models;

use app\modules\admin\models\export\ExportProcessor;

class ExportProcessorTest extends \Codeception\Test\Unit
{
    public function testProcess()
    {
        $input = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'input.txt');
        $expected = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'expected.txt');

        $processor = new ExportProcessor();
        $output = $processor->process($input);

        expect($output)->equals($expected);
    }

    public function testProcess01()
    {
        $input = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR .
            '_data' . DIRECTORY_SEPARATOR . 'input01.txt');
        $expected = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR .
            '_data' . DIRECTORY_SEPARATOR . 'expected01.txt');

        $processor = new ExportProcessor();
        $output = $processor->process($input);

        expect($output)->equals($expected);
    }

    public function testProcess02()
    {
        $input = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR .
            '_data' . DIRECTORY_SEPARATOR . 'input02.txt');
        $expected = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR .
            '_data' . DIRECTORY_SEPARATOR . 'expected02.txt');

        $processor = new ExportProcessor();
        $output = $processor->process($input);

        expect($output)->equals($expected);
    }
}
