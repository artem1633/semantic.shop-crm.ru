<?php

namespace tests\modules\admin\models;

use app\modules\admin\models\creation\FakeTaskDataArrayBuilder;
use app\modules\admin\models\creation\SpecialCharacter;
use app\modules\admin\models\creation\TaskBuilder;
use app\modules\admin\models\creation\TaskDataException;

class TaskDataArrayBuilderTest extends \Codeception\Test\Unit
{

    public $projects;
    public $keywords;
    public $taskData;

    /**
     * @throws TaskDataException
     */
    public function testCreateAndSequence()
    {
        $builder = $this->getBuilder();

        expect($builder->keywords)->equals($this->keywords);

        $this->projects = $builder->createProjects();
        expect($this->projects)->equals([
            'Проект_Сайт:' => 'Проект_Сайт: intellifishing.ru',
            'Проект_Binet:' => 'Проект_Binet: рыбалка'
        ]);

        $this->keywords = $builder->clearKeywordsFromProjectNames($builder->keywords, $this->projects);

        expect($this->keywords)
            ->notContains('Проект_Сайт:');
        expect($this->keywords)
            ->notContains('Проект_Binet:');
        expect($this->keywords)
            ->notContains('Проект_Binet: рыбалка');
        expect($this->keywords)
            ->notContains('Проект_Сайт: intellifishing.ru');


        $this->projects = $builder->clearProjects($this->projects);
        expect($this->projects)->equals([
            'Проект_Сайт:' => 'intellifishing.ru',
            'Проект_Binet:' => 'рыбалка'
        ]);


        $this->keywords = $builder->normalizeKeywords($this->keywords);
        expect($this->keywords)
            ->contains(SpecialCharacter::TWO_SPACE_IN_BEGIN);
        expect($this->keywords)
            ->contains(SpecialCharacter::SPACE);
        expect($this->keywords)
            ->contains(SpecialCharacter::BANG);

        //var_dump($this->keywords);

        $this->taskData = $builder->splitToTasks($this->keywords);
        //var_dump($this->taskData);

    }

    protected function getBuilder()
    {
        $this->keywords = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR .
            '_data' . DIRECTORY_SEPARATOR . 'input.txt');

        return (new FakeTaskDataArrayBuilder(['keywords' => $this->keywords]));
    }

    /**
     * @throws TaskDataException
     */
    public function testBuild()
    {
        $builder = $this->getBuilder();
        $tasksData = $builder->build();
        //var_dump($tasksData);

        expect(count($tasksData))->equals(2);
        expect($builder->getBinetProjectName())
            ->equals('рыбалка');
        expect($builder->getProject()->id)
            ->equals(FakeTaskDataArrayBuilder::PROJECT_ID);

        $taskData = $tasksData[0];

        $taskBuilder = new TaskBuilder([
            'projectId' => $builder->getProject()->id,
            'projectTZBinet' => $builder->getBinetProjectName(),
            'taskText' => $taskData,
        ]);
        $task = $taskBuilder->build();
        //var_dump($task);

        expect($task->project_id)->equals(FakeTaskDataArrayBuilder::PROJECT_ID);
        expect($task->project_TZBinet)->equals('рыбалка');

    }


}
