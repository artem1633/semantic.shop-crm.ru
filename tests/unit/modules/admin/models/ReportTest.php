<?php

namespace tests\modules\admin\models;

use app\modules\admin\models\Period;
use app\modules\admin\models\Report;
use app\modules\admin\models\User;

class ReportTest extends \Codeception\Test\Unit
{
    public function testCreate()
    {
        $report = new Report();
        expect($report)->isInstanceOf(Report::class);
    }

    public function testCountAll()
    {
        $report = new Report();
        expect($report->doneCount)->equals(3);
    }

    public function testCountForUser()
    {
        $report = new Report();
        $report->user = User::findOne(1);
        expect($report->doneCount)->equals(2);
    }

    public function testCountForPeriod()
    {
        $report = new Report();
        $report->period = new Period([
            'start' => '2018-09-01',
            'end' => '2018-09-30',
        ]);

        expect($report->doneCount)->equals(2);
    }

    public function testCountForUserAndPeriod()
    {
        $report = new Report();
        $report->user = User::findOne(1);
        $report->period = new Period([
            'start' => '2018-09-01',
            'end' => '2018-09-30',
        ]);

        expect($report->doneCount)->equals(1);
    }
}
